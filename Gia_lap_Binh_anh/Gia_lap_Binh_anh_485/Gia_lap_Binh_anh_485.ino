
#include "SPI.h"
#include "Adafruit_GFX_AS.h"
#include "Adafruit_ILI9341_STM.h"
#include <GKScroll.h>
#include "logo.h"
//**************************************
#define TFT_CS         PB8
#define TFT_DC         PB6
#define TFT_RST        PB7

#define SD_CS        PB0

Adafruit_ILI9341_STM tft = Adafruit_ILI9341_STM (TFT_CS, TFT_DC, TFT_RST);

//*************************************
#define UP         PA0
#define DOWN       PA3
#define OK         PA1
#define ESC        PA2

//#define UP         PA3
//#define DOWN       PA1
//#define OK         PA0
//#define ESC        PA2

#define RS_485_RE  PA8
//*************************************
int btUP, btDOWN, btOK, btESC;
int lbtUP, lbtDOWN, lbtOK, lbtESC;

volatile boolean up = false;
volatile boolean down = false;
volatile boolean ok = false;
volatile boolean esc = false;

int menuitem = 1, menuitem1 = 1;
int checkmenu;
int page = 1;

int vitribatdau = 0;
String chuoi1 = " CHON TUYEN";
String chuoi2 = " XE VE GARA";
String chuoi3 = " XE HUY DONG";


String chuoi11 = " TUYEN 64";
String chuoi12 = " TUYEN 74";

/////////////////////////////////
String chuoi13 = " TUYEN 14";
String chuoi14 = " TUYEN 14B";
String chuoi15 = " TUYEN 16";

String chuoi16 = " TUYEN 38";
String chuoi17 = " TUYEN 39";
String chuoi18 = " TUYEN 89";

//*********************************

String chuoi11a = " KCN BAC THANG LONG -> PHO NI -> TTTM BINH AN";
String chuoi11b = " TTTM BINH AN -> PHO NI -> KCN BAC THANG LONG";
String chuoi11c = " XE TANG CUONG DI";
String chuoi11d = " XE  TANG CUONG VE";

String chuoi12a = " BX MY DINH -> XUAN KHANH";
String chuoi12b = " XUAN KHANH -> BX MY DINH";
String chuoi12c = " XE  TANG CUONG DI";
String chuoi12d = " XE  TANG CUONG VE";


/////////////////////////////////////
String chuoi13a = " BO HO -> CO NHUE";
String chuoi13b = " CO NHUE -> BO HO";
String chuoi13c = " XE 14 TANG CUONG DI";
String chuoi13d = " XE 14 TANG CUONG VE";



String chuoi14a = " TRAN KHANH DU -> CO NHUE";
String chuoi14b = " CO NHUE -> TRAN KHANH DU";
String chuoi14c = " XE 14B TANG CUONG DI";
String chuoi14d = " XE 14B TANG CUONG VE";

String chuoi15a = " BX MY DINH -> GIA LAM";
String chuoi15b = " GIA LAM -> BX MY DINH";
String chuoi15c = " XE 16 TANG CUONG DI";
String chuoi15d = " XE 16 TANG CUONG VE";

String chuoi16a = " BX NAM THANG LONG -> MAI DONG";
String chuoi16b = " MAI DONG -> BX NAM THANG LONG";
String chuoi16c = " XE 38 TANG CUONG DI";
String chuoi16d = " XE 38 TANG CUONG VE";

String chuoi17a = " CV NGHIA DO -> BV NOI TIET TW";
String chuoi17b = " BV NOI TIET TW -> CV NGHIA DO";
String chuoi17c = " XE 39 TANG CUONG DI";
String chuoi17d = " XE 39 TANG CUONG VE";

String chuoi18a = " BX YEN NGHIA -> BX SON TAY";
String chuoi18b = " BX SON TAY -> BX YEN NGHIA";
String chuoi18c = " XE 89 TANG CUONG DI";
String chuoi18d = " XE 89 TANG CUONG VE";


#define bantin1 "00XVG"
#define bantin2 "00XHD"
#define bantin3 "06401"
#define bantin4 "06402"
#define bantin5 "06403"
#define bantin6 "06404"
#define bantin7 "07401"
#define bantin8 "07402"
#define bantin9 "07403"
#define bantin10 "07404"
#define bantin11 "0141"
#define bantin12 "0142"
#define bantin13 "0143"
#define bantin14 "0144"
//-------------------------------------------------------------------------------

////////////////     0   1   2  3 4  5  6 7 8 9 10 11 1234  15 16 17
char ban_tin_lcs[] = {170, 91, 150, 54, 17, 17, 0, 0, 0, 4, 0, 101, 0, 0, 0, 18, 0, 109};

void chuyen_kenh(int kenh)
{
  char doi_kenh[] = {170, 91, 150, 54, 17, 17, 0, 0, 0, 4, 0, 101, 0, 0, 0, 18, 0, 109};
  doi_kenh[5] = 16 * (kenh / 10 + 1) + kenh % 10;
  doi_kenh[11] = 100 + kenh;
  if (kenh < 10) doi_kenh[17] = 107 + kenh * 2;
  if (kenh > 10) doi_kenh[17] = 133 + (kenh % 10) * 2;
  for (int xx = 0; xx < 18; xx++) Serial1.print((int)doi_kenh[xx]);
}

void setup(void)
{
  pinMode(PB5, OUTPUT);
  digitalWrite(PB5, 1);
  delay(1);

  pinMode(SD_CS, OUTPUT);
  digitalWrite(SD_CS, 0);
  delay(500);

  Serial1.begin(9600);
  Serial3.begin(9600);
  // Set up period

  afio_cfg_debug_ports(AFIO_DEBUG_SW_ONLY);

  pinMode(UP, INPUT_PULLUP);
  pinMode(DOWN, INPUT_PULLUP);
  pinMode(OK, INPUT_PULLUP);
  pinMode(ESC, INPUT_PULLUP);
  pinMode(RS_485_RE, OUTPUT);
  digitalWrite(RS_485_RE, 1);
  pinMode(TFT_RST, OUTPUT);
  digitalWrite(TFT_RST, 0);
  delay(500);
  digitalWrite(TFT_RST, 1);
  delay(500);
  tft.begin();
  tft.setRotation(2);
  drawlogo();
  HardwareTimer timer(2);
  timer.pause();
  timer.setPeriod(100000);
  timer.setChannel1Mode(TIMER_OUTPUT_COMPARE);
  timer.setCompare(TIMER_CH1, 1);
  timer.attachCompare1Interrupt(checktime);
  timer.refresh();

  timer.resume();
  //  //Serial1.end();
}


int dem2 = 0, seconds = 30;
void checktime()
{
  if (esc == true) {
    seconds = 30;
    esc = false;
  }
  seconds++;
  if (seconds == 100) seconds = 30;
  if (dem2 == 1)
  {
    tft.fillRect(0, 200, 240, 70, ILI9341_BLACK);
    tft.setCursor (40, 200);
    tft.print("SENDING...");
    seconds = 0;
    dem2 = 0;
  }
  if (seconds == 5)
  {
    if (esc == true) {
      seconds = 30;
      esc = false;
    }
    tft.fillRect(0, 200, 320, 40, ILI9341_BLACK);
    tft.fillRect(0, 230, 320, 40, ILI9341_BLACK);
    tft.setCursor (40, 230);
    tft.print("SUCCESS");
  }
  if (seconds == 10)
  {
    if (esc == true) {
      seconds = 30;
      esc = false;
    }
    tft.fillRect(0, 200, 320, 40, ILI9341_BLACK);
    tft.fillRect(0, 230, 320, 40, ILI9341_BLACK);
  }
}

//---------------------------------------------------------------------------------
void loop()
{
  drawmenuscroll();

  btUP =  digitalRead(UP);
  btDOWN =  digitalRead(DOWN);
  btOK =  digitalRead(OK);
  btESC =  digitalRead(ESC);

  checkUP();
  checkDOWN();
  checkOK();
  checkESC();

}


//----------------------------------------------------------------------------------
int channel = 1;
  int dem_lcs1 = 10, dem_lcs2 = 10;
void checkUP()
{

  //************************
  if (page == 1)
  {
    if (btUP != lbtUP)
    {
      if (btUP == 0)
      {
        channel++;
        if (channel == 61) channel = 1;
        tft.fillScreen(ILI9341_WHITE);
        tft.setCursor (10, 280);
        tft.setTextColor(ILI9341_BLACK);
        tft.setTextSize(3);
        tft.print(channel);
      }
    }
    lbtUP = btUP;
  }
}

void checkDOWN()
{
  //**************************************
  if (page == 1)
  {
    if (btDOWN != lbtDOWN)
    {
      if (btDOWN == 0)
      {
        //////////////////////
        channel--;
        if (channel == 0) channel = 60;
        tft.fillScreen(ILI9341_WHITE);
        tft.setCursor (10, 280);
        tft.setTextColor(ILI9341_BLACK);
        tft.setTextSize(3);
        tft.print(channel);
      }

    }
    lbtDOWN = btDOWN;
  }
}

void checkOK()
{
  if (btOK != lbtOK)
  {
    if (btOK == 0)
    {
      ok = true;
      dem_lcs1 = 10; dem_lcs2 = 10;;
      tft.fillScreen(ILI9341_WHITE);
      tft.setCursor (10, 280);
      tft.setTextColor(ILI9341_BLACK);
      tft.setTextSize(3);
      tft.print(channel);
      chuyen_kenh(channel);
    }
  }
  lbtOK = btOK;
}

void checkESC()
{
  if (btESC != lbtESC)
  {
    if (btESC == 0)
    {
      /////////

    }
    delay(25);
  }
  lbtESC = btESC;
}
//****************************************************

void drawlogo()
{

  int dem = 0, dem1 = 0;
  tft.fillScreen(ILI9341_WHITE);
  for (int i = 0; i < 68; i++)
    for (int j = 0; j < 80; j++)
    {
      dem1 = pgm_read_word_near(demo + 2 * dem) * 256 + pgm_read_word_near(demo + 2 * dem + 1);

      // draw 4 pixel
      //      tft.drawPixel(20 + j, 20 + i, dem1) ;
      for (int k_1 = 0; k_1 < 2; k_1++)
        for (int k_2 = 0; k_2 < 2; k_2++)
        {
          tft.drawPixel(48 + j * 2 + k_1, 35 + i * 2 + k_2, dem1) ;
        }
      dem++;
      if (dem == 6080) dem = 0;
    };
  //  tft.drawBitmap(35, 178, bitmap3, 168, 21, ILI9341_ORANGE);


  //  tft.drawBitmap(35, 178 , bitmap2, 167, 25, ILI9341_BLUE);
  tft.setCursor (30, 205);
  tft.setTextColor(tft.color565(236, 135, 14));
  tft.setTextSize(3);
  tft.print("HAI VAN BUS");
  tft.setCursor (10, 280);
  tft.setTextColor(ILI9341_BLACK);
  tft.setTextSize(3);
  tft.print("MENU");
  int diem_dung = 1;
  delay(1000);
  tft.fillScreen(ILI9341_WHITE);
  tft.setCursor (10, 280);
  tft.setTextColor(ILI9341_BLACK);
  tft.setTextSize(3);
  tft.print(channel);

  while (1)
  {
        for(int xxx=0;xxx<10;xxx++)
        {
          for(int delay_1=0;delay_1<100;delay_1++){
        btUP =  digitalRead(UP);
    btDOWN =  digitalRead(DOWN);
    btESC =  digitalRead(ESC);
    btOK =  digitalRead(OK);
    checkOK();
    checkUP();
    checkDOWN();
    checkESC();
        delay(20);
        }
        String bantin="#SCFG,90,(32,180806162603,"+String(channel)+",63,40,105.848755,21.003286)";
        Serial1.print(bantin);
        }

    
    
//    if (Serial1.available())
//    {
//      while (1)
//      {
//        if (Serial1.available() == 0) break;
//        tft.setCursor (dem_lcs2, dem_lcs1);
//        tft.setTextColor(ILI9341_BLACK);
//        tft.setTextSize(1);
//        char lcs_c1 = Serial1.read();
//        tft.print((int)lcs_c1);
//        Serial3.print(lcs_c1);
//        dem_lcs1 += 10;
//        delay(1);
//      }
//      dem_lcs2 += 22;
//      dem_lcs1 = 10;
//    }
//    if (Serial3.available())
//    {
//      while (1)
//      {
//        if (Serial3.available() == 0) break;
//        tft.setCursor (dem_lcs2, dem_lcs1);
//        tft.setTextColor(ILI9341_RED);
//        tft.setTextSize(1);
//        char lcs_c2 = Serial3.read();
//        tft.print((int)lcs_c2);
//        Serial1.print(lcs_c2);
//        dem_lcs1 += 10;
//        delay(1);
//      }
//      dem_lcs2 += 22;
//      dem_lcs1 = 10;
//    }
  };
  tft.fillScreen(ILI9341_BLACK);
  tft.setTextColor(ILI9341_WHITE);
  tft.setTextSize(1);
  drawmainmenu();
  delay(10);
}

void drawmainmenu()
{
  //***************************** page1
  if (page == 1)
  {
    if (menuitem == 1)
    {
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi1.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; Serial1.write(bantin1);

      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi2.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 3)
    {
      if ((menuitem == 3) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; Serial1.write(bantin2);
      }
    }
    else
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
      tft.setCursor (10, 90);
      tft.print("3.");
      tft.setCursor (40, 90);
      tft.print(chuoi3.substring(0, 10));
      delay(50);
    }

    tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
  }
  //****************************************************** PAGE 2
  if (page == 2)
  {
    //    if (menuitem == 8){
    //
    //    tft.fillScreen(ILI9341_BLACK);
    //    tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 10);
    //    tft.print("8.");
    //    tft.setCursor (40, 10);
    //    tft.print(chuoi18.substring(0, 10));
    //    delay(50);
    //      //*************************************** khung man hinh
    //  tft.setTextSize(2);
    //  tft.drawRect(5, 280, 115, 40, ILI9341_WHITE);
    //  tft.setCursor (35, 290);
    //  tft.print("ok");
    //  tft.drawRect(120, 280, 115, 40, ILI9341_WHITE);
    //  tft.setCursor (135, 290);
    //  tft.print("cancel");
    //  tft.setTextSize(3);
    //  //  tft.drawRect(111, 270, 220, 250, ILI9341_WHITE);
    //    return;
    //     }
    //    else
    //    {
    //
    //    }
    if (menuitem == 1) {}
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi11.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)  {}
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi12.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3){}
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //    }
    //    tft.setCursor (10, 90);
    //    tft.print("3.");
    //    tft.setCursor (40, 90);
    //    tft.print(chuoi13.substring(0, 10));
    //    delay(50);
    //        //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //        if (menuitem == 4){}
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //    }
    //    tft.setCursor (10, 130);
    //    tft.print("4.");
    //    tft.setCursor (40, 130);
    //    tft.print(chuoi14.substring(0, 10));
    //    delay(50);
    //    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //            if (menuitem == 5){}
    //    else
    //    {
    //      tft.fillRect(0, 160, 320, 40, ILI9341_BLACK);
    //    }
    //    tft.setCursor (10, 170);
    //    tft.print("5.");
    //    tft.setCursor (40, 170);
    //    tft.print(chuoi15.substring(0, 10));
    //    delay(50);
    //    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //         if (menuitem == 6){}
    //    else
    //    {
    //      tft.fillRect(0, 200, 320, 40, ILI9341_BLACK);
    //    }
    //    tft.setCursor (10, 210);
    //    tft.print("6.");
    //    tft.setCursor (40, 210);
    //    tft.print(chuoi16.substring(0, 10));
    //    delay(50);
    //    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //            if (menuitem == 7){}
    //    else
    //    {
    //      tft.fillRect(0, 240, 320, 40, ILI9341_BLACK);
    //    }
    //    tft.setCursor (10, 250);
    //    tft.print("7.");
    //    tft.setCursor (40, 250);
    //    tft.print(chuoi17.substring(0, 10));
    //    delay(50);
    //    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
  }



  //****************************************************** PAGE 3
  if (page == 3)
  {
    if (menuitem == 1) {}
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi11a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin4);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi11b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin5);
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi11c.substring(0, 10));
    //      delay(50);
    //    }
    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin6);
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi11d.substring(0, 10));
    //      delay(50);
    //    }
  }

  //************************************************* page 4
  if (page == 4)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin7);
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi12a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; Serial1.write(bantin8);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi12b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin9);
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi12c.substring(0, 10));
    //      delay(50);
    //    }
    //    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin10);
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi12d.substring(0, 10));
    //      delay(50);
    //    }
  }
  //************************************************* page 5
  if (page == 5)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; Serial1.write(bantin11);
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi13a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; Serial1.write(bantin12);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi13b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin13);
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi13c.substring(0, 10));
    //      delay(50);
    //    }
    //    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin14);
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi13d.substring(0, 10));
    //      delay(50);
    //    }
  }

  //*&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*******************************$&&&&&&&&&&&&&&&&% page6
  if (page == 6)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; Serial1.write(bantin11);
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi14a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; Serial1.write(bantin12);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi14b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin13);
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi14c.substring(0, 10));
    //      delay(50);
    //    }
    //    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin14);
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi14d.substring(0, 10));
    //      delay(50);
    //    }
  }
  //*&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*******************************$&&&&&&&&&&&&&&&&%
  if (page == 7)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; Serial1.write(bantin11);
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi15a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin12);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi15b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin13);
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi15c.substring(0, 10));
    //      delay(50);
    //    }
    //    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin14);
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi15d.substring(0, 10));
    //      delay(50);
    //    }
  }
  //*&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*******************************$&&&&&&&&&&&&&&&&%
  if (page == 8)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; Serial1.write(bantin11);
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi16a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; Serial1.write(bantin12);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi16b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin13);
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi16c.substring(0, 10));
    //      delay(50);
    //    }
    //    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin14);
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi16d.substring(0, 10));
    //      delay(50);
    //    }
  }
  //*&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*******************************$&&&&&&&&&&&&&&&&%
  if (page == 9)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; Serial1.write(bantin11);
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi17a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; Serial1.write(bantin12);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi17b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    ////        ok = false;
    ////        dem2 = 1; Serial1.write(bantin13);
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi17c.substring(0, 10));
    //      delay(50);
    //    }
    //    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    ////        ok = false;
    ////        dem2 = 1; Serial1.write(bantin14);
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi17d.substring(0, 10));
    //      delay(50);
    //    }
  }
  if (page == 10)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; Serial1.write(bantin11);
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi18a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; Serial1.write(bantin12);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi18b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    ////        ok = false;
    ////        dem2 = 1; Serial1.write(bantin13);
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi17c.substring(0, 10));
    //      delay(50);
    //    }
    //    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    ////        ok = false;
    ////        dem2 = 1; Serial1.write(bantin14);
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi17d.substring(0, 10));
    //      delay(50);
    //    }
  }
  //*&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*******************************$&&&&&&&&&&&&&&&&%

  //*************************************** khung man hinh
  tft.setTextSize(2);
  tft.drawRect(5, 280, 115, 40, ILI9341_WHITE);
  tft.setCursor (35, 290);
  tft.print("ok");
  tft.drawRect(120, 280, 115, 40, ILI9341_WHITE);
  tft.setCursor (135, 290);
  tft.print("cancel");
  tft.setTextSize(3);
  //  tft.drawRect(111, 270, 220, 250, ILI9341_WHITE);
}

/*//////////////////////////////////////%^&($(%&))#)#)#)#)_$__%%(&@)#)#(@)__%(#)%#()//////////////////////////////////////////////////////////////
  //////////////////////////////////////%^&($(%&))#)#)#)#)_$__%%(&@)#)#(@)__%(#)%#()//////////////////////////////////////////////////////////////
  //////////////////////////////////////%^&($(%&))#)#)#)#)_$__%%(&@)#)#(@)__%(#)%#()////////////////////////////////////////////////////////////////////////////////////////////////////%^&($(%&))#)#)#)#)_$__%%(&@)#)#(@)__%(#)%#()//////////////////////////////////////////////////////////////
  //////////////////////////////////////%^&($(%&))#)#)#)#)_$__%%(&@)#)#(@)__%(#)%#()//////////////////////////////////////////////////////////////
  //////////////////////////////////////%^&($(%&))#)#)#)#)_$__%%(&@)#)#(@)__%(#)%#()//////////////////////////////////////////////////////////////*/

void drawmenuscroll()
{
  //  if(page==0) drawlogo();
  //********************************************
  if (page == 1)
  {

    //*******************************************
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (45, 10);
      tft.print(ScrollLine(chuoi1, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi2, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; Serial1.write(bantin1);
        //Serial1.end();
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 3)
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
      tft.setCursor (10, 90);
      tft.print("3.");
      tft.setCursor (40, 90);
      tft.print(ScrollLine(chuoi3, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; Serial1.write(bantin2);
        //Serial1.end();
      }
    }
    //************************************
    // if(menuitem ==4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //     tft.print("4.");
    //     tft.setCursor (40, 130);
    //     tft.print(ScrollLine(chuoi4, 5, 10));
    //    delay(100);
    //      if((menuitem ==4)&&(ok ==1))
    //      {
    //        ok = false;
    //        dem2=1;Serial1.write("4");
    //      }
    //    }
  }

  //***********************************************************//////////////////////  PAGE 2
  if (page == 2)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi11, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi12, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi13, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //      }
    //    }
    //        //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi14, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //      }
    //    }
    //        //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 5)
    //    {
    //      tft.fillRect(0, 160, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 170);
    //      tft.print("5.");
    //      tft.setCursor (40, 170);
    //      tft.print(ScrollLine(chuoi15, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //      }
    //    }
    //        //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 6)
    //    {
    //      tft.fillRect(0, 200, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 210);
    //      tft.print("6.");
    //      tft.setCursor (40, 210);
    //      tft.print(ScrollLine(chuoi16, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    ////        //        dem2=1;Serial1.write("3");
    //      }
    //    }
    //        //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 7)
    //    {
    //      tft.fillRect(0, 240, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 250);
    //      tft.print("7.");
    //      tft.setCursor (40, 250);
    //      tft.print(ScrollLine(chuoi17, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    ////        //        dem2=1;Serial1.write("3");
    //      }
    //    }
    //    if (menuitem == 8)
    //    {
    //      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 10);
    //      tft.print("8.");
    //      tft.setCursor (40, 10);
    //      tft.print(ScrollLine(chuoi18, 5, 10));
    //      delay(100);
    //      if ((menuitem == 1) && (ok == 1))
    //      {
    //        ok = false;
    //      }
    //    }

  }
  //***************************************************************
  if (page == 3)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi11a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; Serial1.write(bantin3);

        //Serial1.end();
        //        if(page==1) page=2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi11b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; Serial1.write(bantin4);
        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi11c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin5);
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi11d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin6);
    //      }
    //    }
  }

  //***************************************************************
  if (page == 4)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi12a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; Serial1.write(bantin7);

        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi12b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; Serial1.write(bantin8);

        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi12c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin9);
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi12d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin10);
    //      }
    //    }
  }
  //***************************************************************
  if (page == 5)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi13a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; Serial1.write(bantin11);

        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi13b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; Serial1.write(bantin12);

        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi13c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin13);
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi13d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write(bantin14);
    //      }
    //    }
  }
  //%*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&%**************************************************************
  if (page == 6)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi14a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; Serial1.write("14B1");

        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi14b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; Serial1.write("14B2");

        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi14c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0983");
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi14d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0984");
    //      }
    //    }
  }
  //%*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&%**************************************************************
  if (page == 7)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi15a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; Serial1.write("0161");

        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi15b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; Serial1.write("0162");
        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi15c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0993");
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi15d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0994");
    //      }
    //    }
  }
  //%*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&%**************************************************************
  if (page == 8)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi16a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; Serial1.write("0381");
        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi16b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; Serial1.write("0382");
        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi16c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0643");
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi16d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0644");
    //      }
    //    }
  }
  //%*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&%**************************************************************
  if (page == 9)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi17a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; Serial1.write("0391");
        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi17b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; Serial1.write("0392");

        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi17c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0653");
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi17d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0654");
    //      }
    //    }
  }
  if (page == 10)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi18a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; Serial1.write("0891");
        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi18b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; Serial1.write("0892");

        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi17c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0653");
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi17d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0654");
    //      }
    //    }
  }
}



