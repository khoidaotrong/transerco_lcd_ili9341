#include "SPI.h"
#include "Adafruit_GFX_AS.h"
#include "Adafruit_ILI9341_STM.h"
#include <GKScroll.h>
#include "logo.h"
//**************************************
#define TFT_CS         PA15
#define TFT_DC         PB4
#define TFT_RST        PB3


#define SD_CS        PB0

Adafruit_ILI9341_STM tft = Adafruit_ILI9341_STM (TFT_CS, TFT_DC, TFT_RST);

//*************************************
//#define UP         PB12
//#define DOWN       PB13
//#define OK         PB14
//#define ESC        PB15

#define UP         PB13
#define DOWN       PB14
#define OK         PB15
#define ESC        PB12

#define RS_485_RE  PA8
//*************************************
int btUP, btDOWN, btOK, btESC;
int lbtUP, lbtDOWN, lbtOK, lbtESC;

volatile boolean up = false;
volatile boolean down = false;
volatile boolean ok = false;
volatile boolean esc = false;

int menuitem = 1, menuitem1=1;
int checkmenu;
int page = 1;

int vitribatdau = 0;
String chuoi1 = " CHON TUYEN";
String chuoi2 = " XE VE GARA";
String chuoi3 = " XE HUY DONG";


String chuoi11 = " TUYEN 03";
String chuoi12 = " TUYEN 01";
String chuoi13 = " TUYEN 06A";
String chuoi14 = " TUYEN 06B";
String chuoi15 = " TUYEN 06C";
String chuoi16 = " TUYEN 15";
String chuoi17 = " TUYEN 17";

//*********************************


String chuoi11a = " BX GIAP BAT -> BX GIA LAM";
String chuoi11b = " BX GIA LAM -> BX GIAP BAT";
String chuoi11c = " XE 03 TANG CUONG DI";
String chuoi11d = " XE 03 TANG CUONG VE";

String chuoi12a = " BX GIA LAM -> BX YEN NGHIA";
String chuoi12b = " BX YEN NGHIA -> BX GIA LAM";
String chuoi12c = " XE 01 TANG CUONG DI";
String chuoi12d = " XE 01 TANG CUONG VE";

// +_)(%&$$)##)$(*(^%&^(*%^&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%#^#($   XE 06

String chuoi13a = " BX GIAP BAT -> CAU GIE";
String chuoi13b = " CAU GIE -> BX GIAP BAT";
String chuoi13c = " XE 06A TANG CUONG DI";
String chuoi13d = " XE 06A TANG CUONG VE";

String chuoi14a = " BX GIAP BAT -> HONG VAN (THUONG TIN)";
String chuoi14b = " HONG VAN (THUONG TIN) -> BX GIAP BAT";
String chuoi14c = " XE 06B TANG CUONG DI";
String chuoi14d = " XE 06B TANG CUONG VE";

String chuoi15a = " BX GIAP BAT -> PHU MINH (PHU XUYEN)";
String chuoi15b = " PHU MINH (PHU XUYEN) -> BX GIAP BAT";
String chuoi15c = " XE 06C TANG CUONG DI";
String chuoi15d = " XE 06C TANG CUONG VE";

String chuoi16a = " BX GIA LAM -> PHO NI";
String chuoi16b = " PHO NI -> BX GIA LAM";
String chuoi16c = " XE 06D TANG CUONG DI";
String chuoi16d = " XE 06D TANG CUONG VE"; 

String chuoi17a = " LONG BIEN -> NOI BAI";
String chuoi17b = " NOI BAI -> LONG BIEN";
String chuoi17c = " XE 06E TANG CUONG DI";
String chuoi17d = " XE 06E TANG CUONG VE";

#define  bantin1 "0XVG"
#define bantin2 "0XHD"
#define bantin3 "0031"
#define bantin4 "0032"
#define bantin5 "0033"
#define bantin6 "0034"
#define bantin7 "0011"
#define bantin8 "0012"
#define bantin9 "0013"
#define bantin10 "0014"
#define bantin11 "06A1"
#define bantin12 "06A2"
#define bantin13 "06A3"
#define bantin14 "06A4"
//-------------------------------------------------------------------------------


void setup(void)
{
  pinMode(PB4,OUTPUT);
  digitalWrite(PB4,1);
  delay(1);

  pinMode(SD_CS,OUTPUT);
  digitalWrite(SD_CS,0);
  delay(500);
  delay(300);
  Serial1.begin(9600);
  // Set up period

  afio_cfg_debug_ports(AFIO_DEBUG_SW_ONLY);

  pinMode(UP, INPUT_PULLUP);
  pinMode(DOWN, INPUT_PULLUP);
  pinMode(OK, INPUT_PULLUP);
  pinMode(ESC, INPUT_PULLUP);
  pinMode(RS_485_RE, OUTPUT);
  digitalWrite(RS_485_RE, 1);
  pinMode(TFT_RST, OUTPUT);
    digitalWrite(TFT_RST,0);
  delay(500);
  digitalWrite(TFT_RST,1);
  delay(500);
  tft.begin();
  tft.setRotation(2);
  drawlogo();
  HardwareTimer timer(2);   
  timer.pause();
  timer.setPeriod(100000);
  timer.setChannel1Mode(TIMER_OUTPUT_COMPARE);
  timer.setCompare(TIMER_CH1, 1);
  timer.attachCompare1Interrupt(checktime);
  timer.refresh(); 
   
  timer.resume();

}


int dem2 = 0, seconds = 30;
void checktime()
{
  if(esc==true) {
      seconds=30;
      esc=false;
     }
  seconds++;
  if(seconds==100) seconds=30;
  if (dem2 == 1)
  {
    tft.fillRect(0, 200, 240, 70, ILI9341_BLACK);
    tft.setCursor (40, 200);
    tft.print("SENDING...");
    seconds = 0;
    dem2 = 0;
  }
  if (seconds == 5)
  {
    if(esc==true) {
      seconds=30;
      esc=false;
     }
    tft.fillRect(0, 200, 320, 40, ILI9341_BLACK);
    tft.fillRect(0, 230, 320, 40, ILI9341_BLACK);
    tft.setCursor (40, 230);
    tft.print("SUCCESS");
  }
  if (seconds == 10)
  {
    if(esc==true) {
      seconds=30;
      esc=false;
     }
    tft.fillRect(0, 200, 320, 40, ILI9341_BLACK);
    tft.fillRect(0, 230, 320, 40, ILI9341_BLACK);
  }
}

//---------------------------------------------------------------------------------
void loop()
{
  drawmenuscroll();

  btUP =  digitalRead(UP);
  btDOWN =  digitalRead(DOWN);
  btOK =  digitalRead(OK);
  btESC =  digitalRead(ESC);

  checkUP();
  checkDOWN();
  checkOK();
  checkESC();

}


//----------------------------------------------------------------------------------

void checkUP()
{

  //************************
  if (page == 1)
  {
    if (btUP != lbtUP)
    {
      if (btUP == 0)
      {
        up = true;

      }
      delay(1);
      if (up == 1)
      {
        up = false;
        menuitem--;
        if (menuitem == 0)
          menuitem = 3;
        drawmainmenu();
      }
    }
    lbtUP = btUP;
  }

  //****************************
  if (page == 2)
  {
    if (btUP != lbtUP)
    {
      if (btUP == 0)
      {
        up = true;

      }
      delay(1);
      if (up == 1)
      {
        up = false;
        menuitem--;
        if (menuitem == 0)
          menuitem = 7;
        drawmainmenu();
      }
    }
    lbtUP = btUP;
  }

  
  if ((page>=3)&&(page<=9))
  {
    if (btUP != lbtUP)
    {
      if (btUP == 0)
      {
        up = true;
      }
      delay(1);
      if (up == 1)
      {
        up = false;
        menuitem--;
        if (menuitem == 0)
          menuitem = 2;
        drawmainmenu();
      }
    }
    lbtUP = btUP;
  }
}

void checkDOWN()
{
  //**************************************
  if (page == 1)
  {
    if (btDOWN != lbtDOWN)
    {
      if (btDOWN == 0)
      {
        down = true;

      }
      delay(1);
      if (down == 1)
      {
        down = false;
        menuitem++;
        if (menuitem == 4) menuitem = 1;
        drawmainmenu();
      }
    }
    lbtDOWN = btDOWN;
  }

  //**************************************
  if (page == 2)
  {
    if (btDOWN != lbtDOWN)
    {
      if (btDOWN == 0)
      {
        down = true;

      }
      delay(1);
      if (down == 1)
      {
        down = false;
        menuitem++;
        if (menuitem == 8) menuitem = 1;
        drawmainmenu();
      }
    }
    lbtDOWN = btDOWN;
  }


  if ((page>=3)&&(page<=9))
  {
    if (btDOWN != lbtDOWN)
    {
      if (btDOWN == 0)
      {
        down = true;

      }
      delay(1);
      if (down == 1)
      {
        down = false;
        menuitem++;
        if (menuitem == 3) menuitem = 1;
        drawmainmenu();
      }
    }
    lbtDOWN = btDOWN;
  }
}

void checkOK()
{
  if (btOK != lbtOK)
  {
    if (btOK == 0)
    {
      ok = true;

      if ((page == 2) && (menuitem == 1))
      {
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 3;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();

      }
      else if ((page == 2) && (menuitem == 2))
      {
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 4;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
      else if ((page == 2) && (menuitem == 3))
      {
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 5;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
            else if ((page == 2) && (menuitem == 4))
      {
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 6;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
            else if ((page == 2) && (menuitem == 5))
      {
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 7;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
            else if ((page == 2) && (menuitem == 6))
      {
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 8;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
            else if ((page == 2) && (menuitem == 7))
      {
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 9;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }

      else if ((page == 1) && (menuitem == 1))
      {
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 2;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
    }
    delay(25);
  }
  lbtOK = btOK;
}

void checkESC()
{
  if (btESC != lbtESC)
  {
    if (btESC == 0)
    {
      tft.setTextColor(ILI9341_RED);
      tft.setTextSize(2);
      tft.setCursor (135, 290);
      tft.print("cancel");
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(3);
      esc = true;
      menuitem = 0;
      if (page == 1) drawlogo();
      if (page == 2) { tft.fillRect(0, 0, 320, 280, ILI9341_BLACK); page = 1;}
      if ((page>=3)&&(page<=9)) page = 2;
      drawmainmenu();
      menuitem = 1;
    }
    delay(25);
  }
  lbtESC = btESC;
}
//****************************************************

void drawlogo()
{
  int dem = 0, dem1 = 0;
  tft.fillScreen(ILI9341_WHITE);
  for (int i = 0; i < 72; i++)
    for (int j = 0; j < 76; j++)
    {
      dem1 = pgm_read_word_near(demo + 2 * dem) * 256 + pgm_read_word_near(demo + 2 * dem + 1);
      tft.drawPixel(85 + j, 20 + i, dem1) ;
      dem++;
      if (dem == 6080) dem = 0;
    };
  tft.drawBitmap(35, 160, bitmap3, 168, 21, ILI9341_ORANGE);
  tft.drawBitmap(35, 200 , bitmap2, 167, 25, ILI9341_BLUE);
  tft.setCursor (10, 280);
  tft.setTextColor(ILI9341_BLACK);
  tft.setTextSize(3);
  tft.print("MENU");
  while (1)
  {
    if (digitalRead(OK) == 0) break;
  };
  tft.fillScreen(ILI9341_BLACK);
  tft.setTextColor(ILI9341_WHITE);
  tft.setTextSize(3);
  drawmainmenu();
  delay(10);
}

void drawmainmenu()
{
  //***************************** page1
  if (page == 1)
  {
    if (menuitem == 1)
    {
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi1.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin1);

      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi2.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 3)
    {
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin2);
      }
    }
    else
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
      tft.setCursor (10, 90);
      tft.print("3.");
      tft.setCursor (40, 90);
      tft.print(chuoi3.substring(0, 10));
      delay(50);
    }

    tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
  }
  //****************************************************** PAGE 2
  if (page == 2)
  {
    if (menuitem == 1){}
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi11.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)  {}
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi12.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 3){}
    else
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 90);
    tft.print("3.");
    tft.setCursor (40, 90);
    tft.print(chuoi13.substring(0, 10));
    delay(50);
        //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
        if (menuitem == 4){}
    else
    {
      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 130);
    tft.print("4.");
    tft.setCursor (40, 130);
    tft.print(chuoi14.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
            if (menuitem == 5){}
    else
    {
      tft.fillRect(0, 160, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 170);
    tft.print("5.");
    tft.setCursor (40, 170);
    tft.print(chuoi15.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
            if (menuitem == 6){}
    else
    {
      tft.fillRect(0, 200, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 210);
    tft.print("6.");
    tft.setCursor (40, 210);
    tft.print(chuoi16.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
            if (menuitem == 7){}
    else
    {
      tft.fillRect(0, 240, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 250);
    tft.print("7.");
    tft.setCursor (40, 250);
    tft.print(chuoi17.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    
  }
  
  
  
  //****************************************************** PAGE 3
  if (page == 3)
  {
    if (menuitem == 1){}
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi11a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin4);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi11b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//    if (menuitem == 3)
//    {
//      if ((menuitem == 3) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin5);
//      }
//    }
//    else
//    {
//      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
//      tft.setCursor (10, 90);
//      tft.print("3.");
//      tft.setCursor (40, 90);
//      tft.print(chuoi11c.substring(0, 10));
//      delay(50);
//    }
//    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//    if (menuitem == 4)
//    {
//      if ((menuitem == 4) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin6);
//      }
//    }
//    else
//    {
//      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
//      tft.setCursor (10, 130);
//      tft.print("4.");
//      tft.setCursor (40, 130);
//      tft.print(chuoi11d.substring(0, 10));
//      delay(50);
//    }
  }

  //************************************************* page 4
  if (page == 4)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin7);
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi12a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin8);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi12b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//    if (menuitem == 3)
//    {
//      if ((menuitem == 3) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin9);
//      }
//    }
//    else
//    {
//      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
//      tft.setCursor (10, 90);
//      tft.print("3.");
//      tft.setCursor (40, 90);
//      tft.print(chuoi12c.substring(0, 10));
//      delay(50);
//    }
//    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//    if (menuitem == 4)
//    {
//      if ((menuitem == 4) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin10);
//      }
//    }
//    else
//    {
//      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
//      tft.setCursor (10, 130);
//      tft.print("4.");
//      tft.setCursor (40, 130);
//      tft.print(chuoi12d.substring(0, 10));
//      delay(50);
//    }
  }
  //************************************************* page 5
  if (page == 5)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin11);
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi13a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin12);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi13b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//    if (menuitem == 3) 
//    {
//      if ((menuitem == 3) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin13);
//      }
//    }
//    else
//    {
//      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
//      tft.setCursor (10, 90);
//      tft.print("3.");
//      tft.setCursor (40, 90);
//      tft.print(chuoi13c.substring(0, 10));
//      delay(50);
//    }
//    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//    if (menuitem == 4)
//    {
//      if ((menuitem == 4) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin14);
//      }
//    }
//    else
//    {
//      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
//      tft.setCursor (10, 130);
//      tft.print("4.");
//      tft.setCursor (40, 130);
//      tft.print(chuoi13d.substring(0, 10));
//      delay(50);
//    }
  }

  //*&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*******************************$&&&&&&&&&&&&&&&&% page6
    if (page == 6)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin11);
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi14a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin12);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi14b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//    if (menuitem == 3) 
//    {
//      if ((menuitem == 3) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin13);
//      }
//    }
//    else
//    {
//      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
//      tft.setCursor (10, 90);
//      tft.print("3.");
//      tft.setCursor (40, 90);
//      tft.print(chuoi14c.substring(0, 10));
//      delay(50);
//    }
//    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//    if (menuitem == 4)
//    {
//      if ((menuitem == 4) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin14);
//      }
//    }
//    else
//    {
//      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
//      tft.setCursor (10, 130);
//      tft.print("4.");
//      tft.setCursor (40, 130);
//      tft.print(chuoi14d.substring(0, 10));
//      delay(50);
//    }
  }
    //*&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*******************************$&&&&&&&&&&&&&&&&%
    if (page == 7)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin11);
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi15a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin12);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi15b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//    if (menuitem == 3) 
//    {
//      if ((menuitem == 3) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin13);
//      }
//    }
//    else
//    {
//      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
//      tft.setCursor (10, 90);
//      tft.print("3.");
//      tft.setCursor (40, 90);
//      tft.print(chuoi15c.substring(0, 10));
//      delay(50);
//    }
//    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//    if (menuitem == 4)
//    {
//      if ((menuitem == 4) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin14);
//      }
//    }
//    else
//    {
//      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
//      tft.setCursor (10, 130);
//      tft.print("4.");
//      tft.setCursor (40, 130);
//      tft.print(chuoi15d.substring(0, 10));
//      delay(50);
//    }
  }
    //*&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*******************************$&&&&&&&&&&&&&&&&%
    if (page == 8)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin11);
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi16a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin12);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi16b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//    if (menuitem == 3) 
//    {
//      if ((menuitem == 3) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin13);
//      }
//    }
//    else
//    {
//      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
//      tft.setCursor (10, 90);
//      tft.print("3.");
//      tft.setCursor (40, 90);
//      tft.print(chuoi16c.substring(0, 10));
//      delay(50);
//    }
//    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//    if (menuitem == 4)
//    {
//      if ((menuitem == 4) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin14);
//      }
//    }
//    else
//    {
//      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
//      tft.setCursor (10, 130);
//      tft.print("4.");
//      tft.setCursor (40, 130);
//      tft.print(chuoi16d.substring(0, 10));
//      delay(50);
//    }
  }
    //*&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*******************************$&&&&&&&&&&&&&&&&%
    if (page == 9)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin11);
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi17a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin12);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi17b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//    if (menuitem == 3) 
//    {
//      if ((menuitem == 3) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin13);
//      }
//    }
//    else
//    {
//      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
//      tft.setCursor (10, 90);
//      tft.print("3.");
//      tft.setCursor (40, 90);
//      tft.print(chuoi17c.substring(0, 10));
//      delay(50);
//    }
//    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//    if (menuitem == 4)
//    {
//      if ((menuitem == 4) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin14);
//      }
//    }
//    else
//    {
//      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
//      tft.setCursor (10, 130);
//      tft.print("4.");
//      tft.setCursor (40, 130);
//      tft.print(chuoi17d.substring(0, 10));
//      delay(50);
//    }
  }
    //*&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*******************************$&&&&&&&&&&&&&&&&%

  //*************************************** khung man hinh
  tft.setTextSize(2);
  tft.drawRect(5, 280, 115, 40, ILI9341_WHITE);
  tft.setCursor (35, 290);
  tft.print("ok");
  tft.drawRect(120, 280, 115, 40, ILI9341_WHITE);
  tft.setCursor (135, 290);
  tft.print("cancel");
  tft.setTextSize(3);
  //  tft.drawRect(111, 270, 220, 250, ILI9341_WHITE);
}

//////////////////////////////////////%^&($(%&))#)#)#)#)_$__%%(&@)#)#(@)__%(#)%#()//////////////////////////////////////////////////////////////



//////////////////////////////////////%^&($(%&))#)#)#)#)_$__%%(&@)#)#(@)__%(#)%#()//////////////////////////////////////////////////////////////


//////////////////////////////////////%^&($(%&))#)#)#)#)_$__%%(&@)#)#(@)__%(#)%#()//////////////////////////////////////////////////////////////

void drawmenuscroll()
{
  //  if(page==0) drawlogo();
  //********************************************
  if (page == 1)
  {

    //*******************************************
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (45, 10);
      tft.print(ScrollLine(chuoi1, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi2, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin1);
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 3)
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
      tft.setCursor (10, 90);
      tft.print("3.");
      tft.setCursor (40, 90);
      tft.print(ScrollLine(chuoi3, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin2);
      }
    }
    //************************************
    // if(menuitem ==4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //     tft.print("4.");
    //     tft.setCursor (40, 130);
    //     tft.print(ScrollLine(chuoi4, 5, 10));
    //    delay(100);
    //      if((menuitem ==4)&&(ok ==1))
    //      {
    //        ok = false;
    //        dem2=1;Serial1.write("4");
    //      }
    //    }
  }

  //***********************************************************//////////////////////  PAGE 2
  if (page == 2)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi11, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi12, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //        dem2=1;Serial1.write("2");
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 3)
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
      tft.setCursor (10, 90);
      tft.print("3.");
      tft.setCursor (40, 90);
      tft.print(ScrollLine(chuoi13, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
//        //        dem2=1;Serial1.write("3");
      }
    }
        //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 4)
    {
      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
      tft.setCursor (10, 130);
      tft.print("4.");
      tft.setCursor (40, 130);
      tft.print(ScrollLine(chuoi14, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
//        //        dem2=1;Serial1.write("3");
      }
    }
        //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 5)
    {
      tft.fillRect(0, 160, 320, 40, ILI9341_RED);
      tft.setCursor (10, 170);
      tft.print("5.");
      tft.setCursor (40, 170);
      tft.print(ScrollLine(chuoi15, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
//        //        dem2=1;Serial1.write("3");
      }
    }
        //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 6)
    {
      tft.fillRect(0, 200, 320, 40, ILI9341_RED);
      tft.setCursor (10, 210);
      tft.print("6.");
      tft.setCursor (40, 210);
      tft.print(ScrollLine(chuoi16, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
//        //        dem2=1;Serial1.write("3");
      }
    }
        //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 7)
    {
      tft.fillRect(0, 240, 320, 40, ILI9341_RED);
      tft.setCursor (10, 250);
      tft.print("7.");
      tft.setCursor (40, 250);
      tft.print(ScrollLine(chuoi17, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
//        //        dem2=1;Serial1.write("3");
      }
    }
  }
  //***************************************************************
  if (page == 3)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi11a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin3);
        //        if(page==1) page=2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi11b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin4);
      }
    }
    //***********************************************
//    if (menuitem == 3)
//    {
//      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 90);
//      tft.print("3.");
//      tft.setCursor (40, 90);
//      tft.print(ScrollLine(chuoi11c, 5, 10));
//      delay(100);
//      if ((menuitem == 3) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin5);
//      }
//    }
//    //************************************
//    if (menuitem == 4)
//    {
//      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 130);
//      tft.print("4.");
//      tft.setCursor (40, 130);
//      tft.print(ScrollLine(chuoi11d, 5, 10));
//      delay(100);
//      if ((menuitem == 4) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin6);
//      }
//    }
  }

  //***************************************************************
  if (page == 4)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi12a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin7);
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi12b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin8);
      }
    }
    //***********************************************
//    if (menuitem == 3)
//    {
//      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 90);
//      tft.print("3.");
//      tft.setCursor (40, 90);
//      tft.print(ScrollLine(chuoi12c, 5, 10));
//      delay(100);
//      if ((menuitem == 3) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin9);
//      }
//    }
//    //************************************
//    if (menuitem == 4)
//    {
//      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 130);
//      tft.print("4.");
//      tft.setCursor (40, 130);
//      tft.print(ScrollLine(chuoi12d, 5, 10));
//      delay(100);
//      if ((menuitem == 4) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin10);
//      }
//    }
  }
  //***************************************************************
  if (page == 5)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi13a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin11);
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi13b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin12);
      }
    }
    //***********************************************
//    if (menuitem == 3)
//    {
//      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 90);
//      tft.print("3.");
//      tft.setCursor (40, 90);
//      tft.print(ScrollLine(chuoi13c, 5, 10));
//      delay(100);
//      if ((menuitem == 3) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin13);
//      }
//    }
//    //************************************
//    if (menuitem == 4)
//    {
//      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 130);
//      tft.print("4.");
//      tft.setCursor (40, 130);
//      tft.print(ScrollLine(chuoi13d, 5, 10));
//      delay(100);
//      if ((menuitem == 4) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write(bantin14);
//      }
//    }
  }
  //%*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&%**************************************************************
    if (page == 6)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);   
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi14a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write("06B1");
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi14b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write("06B2");
      }
    }
    //***********************************************
//    if (menuitem == 3)
//    {
//      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 90);
//      tft.print("3.");
//      tft.setCursor (40, 90);
//      tft.print(ScrollLine(chuoi14c, 5, 10));
//      delay(100);
//      if ((menuitem == 3) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write("0623");
//      }
//    }
//    //************************************
//    if (menuitem == 4)
//    {
//      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 130);
//      tft.print("4.");
//      tft.setCursor (40, 130);
//      tft.print(ScrollLine(chuoi14d, 5, 10));
//      delay(100);
//      if ((menuitem == 4) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write("0624");
//      }
//    }
  }
    //%*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&%**************************************************************
    if (page == 7)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi15a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write("06C1");
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi15b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write("06C2");
      }
    }
    //***********************************************
//    if (menuitem == 3)
//    {
//      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 90);
//      tft.print("3.");
//      tft.setCursor (40, 90);
//      tft.print(ScrollLine(chuoi15c, 5, 10));
//      delay(100);
//      if ((menuitem == 3) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write("0633");
//      }
//    }
//    //************************************
//    if (menuitem == 4)
//    {
//      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 130);
//      tft.print("4.");
//      tft.setCursor (40, 130);
//      tft.print(ScrollLine(chuoi15d, 5, 10));
//      delay(100);
//      if ((menuitem == 4) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write("0634");
//      }
//    }
  }
    //%*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&%**************************************************************
    if (page == 8)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi16a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write("0151");
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi16b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write("0152");
      }
    }
    //***********************************************
//    if (menuitem == 3)
//    {
//      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 90);
//      tft.print("3.");
//      tft.setCursor (40, 90);
//      tft.print(ScrollLine(chuoi16c, 5, 10));
//      delay(100);
//      if ((menuitem == 3) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write("0643");
//      }
//    }
//    //************************************
//    if (menuitem == 4)
//    {
//      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 130);
//      tft.print("4.");
//      tft.setCursor (40, 130);
//      tft.print(ScrollLine(chuoi16d, 5, 10));
//      delay(100);
//      if ((menuitem == 4) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write("0644");
//      }
//    }
  }
    //%*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&%**************************************************************
    if (page == 9)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi17a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write("0171");
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi17b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write("0172");
      }
    }
    //***********************************************
//    if (menuitem == 3)
//    {
//      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 90);
//      tft.print("3.");
//      tft.setCursor (40, 90);
//      tft.print(ScrollLine(chuoi17c, 5, 10));
//      delay(100);
//      if ((menuitem == 3) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write("0653");
//      }
//    }
//    //************************************
//    if (menuitem == 4)
//    {
//      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 130);
//      tft.print("4.");
//      tft.setCursor (40, 130);
//      tft.print(ScrollLine(chuoi17d, 5, 10));
//      delay(100);
//      if ((menuitem == 4) && (ok == 1))
//      {
//        ok = false;
//        dem2 = 1; Serial1.write("0654");
//      }
//    }
  }
}




