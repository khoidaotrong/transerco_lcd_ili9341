#include "SPI.h"
#include "Adafruit_GFX_AS.h"
#include "Adafruit_ILI9341_STM.h"
#include <GKScroll.h>
#include "logo.h"
//**************************************
#define TFT_CS         PA15
#define TFT_DC         PB4
#define TFT_RST        PB3

Adafruit_ILI9341_STM tft = Adafruit_ILI9341_STM (TFT_CS, TFT_DC, TFT_RST);

//*************************************
//#define UP         PB12
//#define DOWN       PB13
//#define OK         PB14
//#define ESC        PB15

#define UP         PB13
#define DOWN       PB14
#define OK         PB15
#define ESC        PB12

#define RS_485_RE  PA8
//*************************************
int btUP, btDOWN, btOK, btESC;
int lbtUP, lbtDOWN, lbtOK, lbtESC;

volatile boolean up = false;
volatile boolean down = false;
volatile boolean ok = false;
volatile boolean esc = false;

int menuitem = 1;
int checkmenu;
int page = 1;

int vitribatdau = 0;
String chuoi1 = " CHON TUYEN";
String chuoi2 = " XE VE GARA";
String chuoi3 = " XE HUY DONG";


String chuoi12 = " TUYEN 01";
String chuoi11 = " TUYEN 03";
String chuoi13 = " TUYEN 06";

//*********************************
String chuoi12a = " BX GIA LAM -> BX YEN NGHIA";
String chuoi12b = "  BX YEN NGHIA -> BX GIA LAM";
String chuoi12c = " XE 01 TANG CUONG DI";
String chuoi12d = " XE 01 TANG CUONG VE";


String chuoi11a = " BX GIAP BAT -> BX GIA LAM";
String chuoi11b = " BX GIA LAM -> BX GIAP BAT";
String chuoi11c = " XE 03 TANG CUONG DI";
String chuoi11d = " XE 03 TANG CUONG VE";

String chuoi13a = " MAI DONG -> BX MY DINH";
String chuoi13b = " BX MY DINH -> MAI DONG";
String chuoi13c = " XE 30 TANG CUONG DI";
String chuoi13d = " XE 30 TANG CUONG VE";

#define  bantin1 "0XVG"
#define bantin2 "0XHD"
#define bantin3 "0031"
#define bantin4 "0032"
#define bantin5 "0033"
#define bantin6 "0034"
#define bantin7 "0031"
#define bantin8 "0032"
#define bantin9 "0033"
#define bantin10 "0034"
#define bantin11 "0031"
#define bantin12 "0032"
#define bantin13 "0033"
#define bantin14 "0034"
//-------------------------------------------------------------------------------


void setup(void)
{
  delay(300);
  Serial1.begin(9600);
  // Set up period

  afio_cfg_debug_ports(AFIO_DEBUG_SW_ONLY);

  pinMode(UP, INPUT_PULLUP);
  pinMode(DOWN, INPUT_PULLUP);
  pinMode(OK, INPUT_PULLUP);
  pinMode(ESC, INPUT_PULLUP);
  pinMode(RS_485_RE, OUTPUT);
  digitalWrite(RS_485_RE, 1);
  pinMode(TFT_RST, OUTPUT);
    digitalWrite(TFT_RST,0);
  delay(300);
  digitalWrite(TFT_RST,1);
  delay(300);
  tft.begin();
  tft.setRotation(2);
  drawlogo();
  HardwareTimer timer(2);   
  timer.pause();
  timer.setPeriod(100000);
  timer.setChannel1Mode(TIMER_OUTPUT_COMPARE);
  timer.setCompare(TIMER_CH1, 1);
  timer.attachCompare1Interrupt(checktime);
  timer.refresh(); 
   
  timer.resume();

}

int dem2 = 0, seconds = 30;
void checktime()
{
  if(esc==true) {
      seconds=30;
      esc=false;
     }
  seconds++;
  if (dem2 == 1)
  {
    tft.fillRect(0, 200, 240, 70, ILI9341_BLACK);
    tft.setCursor (40, 200);
    tft.print("SENDING...");
    seconds = 0;
    dem2 = 0;
  }
  if (seconds == 5)
  {
    if(esc==true) {
      seconds=30;
      esc=false;
     }
    tft.fillRect(0, 200, 320, 40, ILI9341_BLACK);
    tft.fillRect(0, 230, 320, 40, ILI9341_BLACK);
    tft.setCursor (40, 230);
    tft.print("SUCCESS");
  }
  if (seconds == 10)
  {
    if(esc==true) {
      seconds=30;
      esc=false;
     }
    tft.fillRect(0, 200, 320, 40, ILI9341_BLACK);
    tft.fillRect(0, 230, 320, 40, ILI9341_BLACK);
  }
}

//---------------------------------------------------------------------------------
void loop()
{
  drawmenuscroll();

  btUP =  digitalRead(UP);
  btDOWN =  digitalRead(DOWN);
  btOK =  digitalRead(OK);
  btESC =  digitalRead(ESC);

  checkUP();
  checkDOWN();
  checkOK();
  checkESC();





}
//----------------------------------------------------------------------------------
void checkUP()
{

  //************************
  if (page == 1)
  {
    if (btUP != lbtUP)
    {
      if (btUP == 0)
      {
        up = true;

      }
      delay(1);
      if (up == 1)
      {
        up = false;
        menuitem--;
        if (menuitem == 0)
          menuitem = 3;
        drawmainmenu();
      }
    }
    lbtUP = btUP;
  }

  //****************************
  if (page == 2)
  {
    if (btUP != lbtUP)
    {
      if (btUP == 0)
      {
        up = true;

      }
      delay(1);
      if (up == 1)
      {
        up = false;
        menuitem--;
        if (menuitem == 0)
          menuitem = 3;
        drawmainmenu();
      }
    }
    lbtUP = btUP;
  }
  //*********************
  if (page == 3)
  {
    if (btUP != lbtUP)
    {
      if (btUP == 0)
      {
        up = true;

      }
      delay(1);
      if (up == 1)
      {
        up = false;
        menuitem--;
        if (menuitem == 0)
          menuitem = 4;
        drawmainmenu();
      }
    }
    lbtUP = btUP;
  }
  //*********************
  if (page == 4)
  {
    if (btUP != lbtUP)
    {
      if (btUP == 0)
      {
        up = true;

      }
      delay(1);
      if (up == 1)
      {
        up = false;
        menuitem--;
        if (menuitem == 0)
          menuitem = 4;
        drawmainmenu();
      }
    }
    lbtUP = btUP;
  }
  //*********************
  if (page == 5)
  {
    if (btUP != lbtUP)
    {
      if (btUP == 0)
      {
        up = true;

      }
      delay(1);
      if (up == 1)
      {
        up = false;
        menuitem--;
        if (menuitem == 0)
          menuitem = 4;
        drawmainmenu();
      }
    }
    lbtUP = btUP;
  }
}

void checkDOWN()
{
  //**************************************
  if (page == 1)
  {
    if (btDOWN != lbtDOWN)
    {
      if (btDOWN == 0)
      {
        down = true;

      }
      delay(1);
      if (down == 1)
      {
        down = false;
        menuitem++;
        if (menuitem == 4) menuitem = 1;
        drawmainmenu();
      }
    }
    lbtDOWN = btDOWN;
  }

  //**************************************
  if (page == 2)
  {
    if (btDOWN != lbtDOWN)
    {
      if (btDOWN == 0)
      {
        down = true;

      }
      delay(1);
      if (down == 1)
      {
        down = false;
        menuitem++;
        if (menuitem == 4) menuitem = 1;
        drawmainmenu();
      }
    }
    lbtDOWN = btDOWN;
  }

  //**************************************
  if (page == 3)
  {
    if (btDOWN != lbtDOWN)
    {
      if (btDOWN == 0)
      {
        down = true;

      }
      delay(1);
      if (down == 1)
      {
        down = false;
        menuitem++;
        if (menuitem == 5) menuitem = 1;
        drawmainmenu();
      }
    }
    lbtDOWN = btDOWN;
  }
  //**************************************
  if (page == 4)
  {
    if (btDOWN != lbtDOWN)
    {
      if (btDOWN == 0)
      {
        down = true;

      }
      delay(1);
      if (down == 1)
      {
        down = false;
        menuitem++;
        if (menuitem == 5) menuitem = 1;
        drawmainmenu();
      }
    }
    lbtDOWN = btDOWN;
  }
  //**************************************
  if (page == 5)
  {
    if (btDOWN != lbtDOWN)
    {
      if (btDOWN == 0)
      {
        down = true;

      }
      delay(1);
      if (down == 1)
      {
        down = false;
        menuitem++;
        if (menuitem == 5) menuitem = 1;
        drawmainmenu();
      }
    }
    lbtDOWN = btDOWN;
  }
}

void checkOK()
{
  if (btOK != lbtOK)
  {
    if (btOK == 0)
    {
      ok = true;

      if ((page == 2) && (menuitem == 1))
      {
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 3;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 240, ILI9341_BLACK);
        drawmainmenu();

      }
      else if ((page == 2) && (menuitem == 2))
      {
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 4;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 240, ILI9341_BLACK);
        drawmainmenu();
      }
      else if ((page == 2) && (menuitem == 3))
      {
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 5;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 240, ILI9341_BLACK);
        drawmainmenu();
      }

      else if ((page == 1) && (menuitem == 1))
      {
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 3;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 240, ILI9341_BLACK);
        drawmainmenu();
      }
    }
    delay(25);
  }
  lbtOK = btOK;
}

void checkESC()
{
  if (btESC != lbtESC)
  {
    if (btESC == 0)
    {
      tft.setTextColor(ILI9341_RED);
      tft.setTextSize(2);
      tft.setCursor (135, 290);
      tft.print("cancel");
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(3);
      esc = true;
      menuitem = 0;
      if (page == 1) drawlogo();
      if (page == 2) page = 1;
      if (page == 3) page = 1;
      if (page == 4) page = 2;
      if (page == 5) page = 2;
      drawmainmenu();
      menuitem = 1;
    }
    delay(25);
  }
  lbtESC = btESC;
}
//****************************************************

void drawlogo()
{
  int dem = 0, dem1 = 0;
  tft.fillScreen(ILI9341_WHITE);
  for (int i = 0; i < 72; i++)
    for (int j = 0; j < 76; j++)
    {
      dem1 = pgm_read_word_near(demo + 2 * dem) * 256 + pgm_read_word_near(demo + 2 * dem + 1);
      tft.drawPixel(85 + j, 20 + i, dem1) ;
      dem++;
      if (dem == 6080) dem = 0;
    };
  tft.drawBitmap(35, 160, bitmap3, 168, 21, ILI9341_ORANGE);
  tft.drawBitmap(35, 200 , bitmap2, 167, 25, ILI9341_BLUE);
  tft.setCursor (10, 280);
  tft.setTextColor(ILI9341_BLACK);
  tft.setTextSize(3);
  tft.print("MENU");
  while (1)
  {
    if (digitalRead(OK) == 0) break;
  };
  tft.fillScreen(ILI9341_BLACK);
  tft.setTextColor(ILI9341_WHITE);
  tft.setTextSize(3);
  drawmainmenu();
  delay(10);
}

void drawmainmenu()
{
  //***************************** page1
  if (page == 1)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //        dem2=1;Serial1.write("0161");
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi1.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin1);

      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi2.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 3)
    {
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin2);
      }
    }
    else
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
      tft.setCursor (10, 90);
      tft.print("3.");
      tft.setCursor (40, 90);
      tft.print(chuoi3.substring(0, 10));
      delay(50);
    }
    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    // if(menuitem ==4)
    //    {
    //      if((menuitem ==4)&&(ok ==1))
    //      {
    //        ok = false;
    //        dem2=1;Serial1.write("4");
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //    tft.print("4.");
    //    tft.setCursor (40, 130);
    //    tft.print(chuoi4.substring(0,10));
    //    delay(50);
    //    }
    tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
  }
  //****************************************************** PAGE 2
  if (page == 2)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //        dem2=1;Serial1.write(bantin3);
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi11.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //        dem2=1;Serial1.write("2");
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi12.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 3)
    {
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
        //        dem2=1;Serial1.write("3");
      }
    }
    else
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 90);
    tft.print("3.");
    tft.setCursor (40, 90);
    tft.print(chuoi13.substring(0, 10));
    delay(50);
    tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
  }
  //****************************************************** PAGE 3
  if (page == 3)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin3);
        if (page == 1) page = 2;
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi11a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin4);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi11b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 3)
    {
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin5);
      }
    }
    else
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
      tft.setCursor (10, 90);
      tft.print("3.");
      tft.setCursor (40, 90);
      tft.print(chuoi11c.substring(0, 10));
      delay(50);
    }
    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 4)
    {
      if ((menuitem == 4) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin6);
      }
    }
    else
    {
      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
      tft.setCursor (10, 130);
      tft.print("4.");
      tft.setCursor (40, 130);
      tft.print(chuoi11d.substring(0, 10));
      delay(50);
    }
  }

  //************************************************* page 4
  if (page == 4)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin7);
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi12a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin8);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi12b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 3)
    {
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin9);
      }
    }
    else
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
      tft.setCursor (10, 90);
      tft.print("3.");
      tft.setCursor (40, 90);
      tft.print(chuoi12c.substring(0, 10));
      delay(50);
    }
    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 4)
    {
      if ((menuitem == 4) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin10);
      }
    }
    else
    {
      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
      tft.setCursor (10, 130);
      tft.print("4.");
      tft.setCursor (40, 130);
      tft.print(chuoi12d.substring(0, 10));
      delay(50);
    }
  }
  //************************************************* page 5
  if (page == 5)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin11);
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi13a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin12);
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi13b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 3) 
    {
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin13);
      }
    }
    else
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
      tft.setCursor (10, 90);
      tft.print("3.");
      tft.setCursor (40, 90);
      tft.print(chuoi13a.substring(0, 10));
      delay(50);
    }
    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 4)
    {
      if ((menuitem == 4) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin14);
      }
    }
    else
    {
      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
      tft.setCursor (10, 130);
      tft.print("4.");
      tft.setCursor (40, 130);
      tft.print(chuoi13b.substring(0, 10));
      delay(50);
    }
  }
  //*************************************** khung man hinh
  tft.setTextSize(2);
  tft.drawRect(5, 280, 115, 40, ILI9341_WHITE);
  tft.setCursor (35, 290);
  tft.print("ok");
  tft.drawRect(120, 280, 115, 40, ILI9341_WHITE);
  tft.setCursor (135, 290);
  tft.print("cancel");
  tft.setTextSize(3);
  //  tft.drawRect(111, 270, 220, 250, ILI9341_WHITE);
}





/////////////////////%^&($(%&))#)#)#)#)_$__%%(&@)#)#(@)__%(#)%#()//////////////////////////////////////////////////////////////




void drawmenuscroll()
{
  //  if(page==0) drawlogo();
  //********************************************
  if (page == 1)
  {

    //*******************************************
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (45, 10);
      tft.print(ScrollLine(chuoi1, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi2, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin1);
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 3)
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
      tft.setCursor (10, 90);
      tft.print("3.");
      tft.setCursor (40, 90);
      tft.print(ScrollLine(chuoi3, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin2);
      }
    }
    //************************************
    // if(menuitem ==4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //     tft.print("4.");
    //     tft.setCursor (40, 130);
    //     tft.print(ScrollLine(chuoi4, 5, 10));
    //    delay(100);
    //      if((menuitem ==4)&&(ok ==1))
    //      {
    //        ok = false;
    //        dem2=1;Serial1.write("4");
    //      }
    //    }
  }

  //***********************************************************
  if (page == 2)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi11, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi12, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //        dem2=1;Serial1.write("2");
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 3)
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
      tft.setCursor (10, 90);
      tft.print("3.");
      tft.setCursor (40, 90);
      tft.print(ScrollLine(chuoi13, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
        //        dem2=1;Serial1.write("3");
      }
    }
  }
  //***************************************************************
  if (page == 3)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi11a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin3);
        //        if(page==1) page=2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi11b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin4);
      }
    }
    //***********************************************
    if (menuitem == 3)
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
      tft.setCursor (10, 90);
      tft.print("3.");
      tft.setCursor (40, 90);
      tft.print(ScrollLine(chuoi11c, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin5);
      }
    }
    //************************************
    if (menuitem == 4)
    {
      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
      tft.setCursor (10, 130);
      tft.print("4.");
      tft.setCursor (40, 130);
      tft.print(ScrollLine(chuoi11d, 5, 10));
      delay(100);
      if ((menuitem == 4) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin6);
      }
    }
  }

  //***************************************************************
  if (page == 4)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi12a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin7);
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi12b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin8);
      }
    }
    //***********************************************
    if (menuitem == 3)
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
      tft.setCursor (10, 90);
      tft.print("3.");
      tft.setCursor (40, 90);
      tft.print(ScrollLine(chuoi12c, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin9);
      }
    }
    //************************************
    if (menuitem == 4)
    {
      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
      tft.setCursor (10, 130);
      tft.print("4.");
      tft.setCursor (40, 130);
      tft.print(ScrollLine(chuoi12d, 5, 10));
      delay(100);
      if ((menuitem == 4) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin10);
      }
    }
  }
  //***************************************************************
  if (page == 5)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi13a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin11);
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi13b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin12);
      }
    }
    //***********************************************
    if (menuitem == 3)
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
      tft.setCursor (10, 90);
      tft.print("3.");
      tft.setCursor (40, 90);
      tft.print(ScrollLine(chuoi13c, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin13);
      }
    }
    //************************************
    if (menuitem == 4)
    {
      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
      tft.setCursor (10, 130);
      tft.print("4.");
      tft.setCursor (40, 130);
      tft.print(ScrollLine(chuoi13d, 5, 10));
      delay(100);
      if ((menuitem == 4) && (ok == 1))
      {
        ok = false;
        dem2 = 1; Serial1.write(bantin14);
      }
    }
  }
}



