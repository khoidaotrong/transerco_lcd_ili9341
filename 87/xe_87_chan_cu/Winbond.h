#include <SPI.h>

SPIClass SPI_2(2);

#define RE_DE_485 PA8

// WinBond flash commands
#define WB_WRITE_ENABLE       0x06
#define WB_WRITE_DISABLE      0x04
#define WB_CHIP_ERASE         0xc7
#define SECTOR_ERASE          0x20  //sector erase 4KB
#define WB_READ_STATUS_REG_1  0x05
#define WB_READ_DATA          0x03
#define WB_PAGE_PROGRAM       0x02
#define WB_JEDEC_ID           0x9f

//String g_command;


void print_page_bytes(byte *page_buffer) {
  char buf[10];
  for (int i = 0; i < 16; ++i) {
    for (int j = 0; j < 16; ++j) {
      sprintf(buf, "%02x", page_buffer[i * 16 + j]);
      Serial1.print(buf);
    }
    Serial1.println();
  }
}

 void not_busy(void) {
  digitalWrite(SS1, HIGH);
  digitalWrite(SS1, LOW);
  SPI_2.transfer(WB_READ_STATUS_REG_1);       
  while (SPI_2.transfer(0) & 1) {}; 
  digitalWrite(SS1, HIGH);  
}

void _get_jedec_id(byte *b1, byte *b2, byte *b3) {
  digitalWrite(SS1, HIGH);
  digitalWrite(SS1, LOW);
  SPI_2.transfer(WB_JEDEC_ID);
  *b1 = SPI_2.transfer(0); // manufacturer id
  *b2 = SPI_2.transfer(0); // memory type
  *b3 = SPI_2.transfer(0); // capacity
  digitalWrite(SS1, HIGH);
  not_busy();
}  

void get_jedec_id(void) {
  Serial1.println("command: get_jedec_id");
  byte b1, b2, b3;
  _get_jedec_id(&b1, &b2, &b3);
  char buf[128];
  sprintf(buf, "Manufacturer ID: %02xh\nMemory Type: %02xh\nCapacity: %02xh",
    b1, b2, b3);
  Serial1.println(buf);
  Serial1.println("Ready");
} 

void _chip_erase(void) {
  digitalWrite(SS1, HIGH);
  digitalWrite(SS1, LOW);  
  SPI_2.transfer(WB_WRITE_ENABLE);
  digitalWrite(SS1, HIGH);
  digitalWrite(SS1, LOW);  
  SPI_2.transfer(WB_CHIP_ERASE);
  digitalWrite(SS1, HIGH);
  not_busy();
}

void chip_erase(void) {
  Serial1.println("command: chip_erase");
  _chip_erase();
  Serial1.println("Ready");
  //set = 1;
}

void _read_page(word page_number, byte *page_buffer) {
  digitalWrite(SS1, HIGH);
  digitalWrite(SS1, LOW);
  SPI_2.transfer(WB_READ_DATA);
  // Construct the 24-bit address from the 16-bit page
  // number and 0x00, since we will read 256 bytes (one
  // page).
  SPI_2.transfer((page_number >> 8) & 0xFF);
  SPI_2.transfer((page_number >> 0) & 0xFF);
  SPI_2.transfer(0);
  for (int i = 0; i < 256; ++i) {
    page_buffer[i] = SPI_2.transfer(0);
  }
  digitalWrite(SS1, HIGH);
  not_busy();
}

void _read_byte(word page_number, byte *byte_buffer, byte offset) {
  digitalWrite(SS1, HIGH);
  digitalWrite(SS1, LOW);
  SPI_2.transfer(WB_READ_DATA);
  // Construct the 24-bit address from the 16-bit page
  // number and 0x00, since we will read 256 bytes (one
  // page).
  SPI_2.transfer((page_number >> 8) & 0xFF);
  SPI_2.transfer((page_number >> 0) & 0xFF);
  SPI_2.transfer(0);
  for (int i = 0; i < 255; i++) {
    byte_buffer[i] = SPI_2.transfer(0);
    if(i == offset){
      break;
    }
  }
  digitalWrite(SS1, HIGH);
  not_busy();
}
 
/*
 * See the timing diagram in section 9.2.21 of the
 * data sheet, "Page Program (02h)".
 */
void _write_page(word page_number, byte *page_buffer) {
  digitalWrite(SS1, HIGH);
  digitalWrite(SS1, LOW);  
  SPI_2.transfer(WB_WRITE_ENABLE);
  digitalWrite(SS1, HIGH);
  digitalWrite(SS1, LOW);  
  SPI_2.transfer(WB_PAGE_PROGRAM);
  SPI_2.transfer((page_number >>  8) & 0xFF);
  SPI_2.transfer((page_number >>  0) & 0xFF);
  SPI_2.transfer(0);
  for (int i = 0; i < 256; ++i) {
    SPI_2.transfer(page_buffer[i]);
  }
  digitalWrite(SS1, HIGH);
  /* See notes on rev 2
  digitalWrite(SS, LOW);  
  SPI_2.transfer(WB_WRITE_DISABLE);
  digitalWrite(SS, HIGH);
  */
  not_busy();
}

void read_page(unsigned int page_number) {
  char buf[80];
  sprintf(buf, "command: read_page(%04xh)", page_number);
  Serial1.println(buf);
  byte page_buffer[256];
  _read_page(page_number, page_buffer);
  print_page_bytes(page_buffer);
  Serial1.println("Ready");
}

void read_all_pages(void) {
  Serial1.println("command: read_all_pages");
  byte page_buffer[256];
  for (int i = 0; i < 4096; ++i) {
    _read_page(i, page_buffer);
    print_page_bytes(page_buffer);
  }
  Serial1.println("Ready");
}

void read_byte(word page, byte offset){
  char buf[80];
  sprintf(buf, "command: read_byte(%04xh, %04xh)", page, offset);
  Serial1.println(buf);
  byte byte_buffer[256];
  _read_byte(page, byte_buffer, offset);
  //print_page_bytes(page_buffer);
  Serial1.println(byte_buffer[offset]); 
  //not_busy();

}

void write_byte(word page, byte offset, byte databyte) {
  char buf[80];
  sprintf(buf, "command: write_byte(%04xh, %04xh, %02xh)", page, offset, databyte);
  Serial1.println(buf);
  byte page_data[256];
  _read_page(page, page_data);
  page_data[offset] = databyte;
  _write_page(page, page_data);
  Serial1.println("Ready");
  //not_busy();
}

void eraseSector(uint32_t addr_start)      // addr_start = 0 - 2048
{
  addr_start = addr_start<<12;     
  digitalWrite(SS1, HIGH);
  digitalWrite(SS1, LOW); 
  SPI_2.transfer(WB_WRITE_ENABLE);
  digitalWrite(SS1, HIGH);
  digitalWrite(SS1, LOW); 
  SPI_2.transfer(SECTOR_ERASE);
  SPI_2.transfer(addr_start>>16);
  SPI_2.transfer(addr_start>>8);
  SPI_2.transfer(addr_start);
  digitalWrite(SS1, HIGH);
  not_busy();
}

void write_numberP10(word page, const unsigned char *data){       // 64byte
  for(int i = 0; i < 64; i++){
      write_byte(page,i,data[i]);
  }
}

void write_contentP10(word page1, word page2, const unsigned char *data){             // 480 byte
  for(int i = 0; i < 256; i++){
    write_byte(page1,i,data[i]);
  }
  for(int i = 0; i < 256; i++){
    write_byte(page2,i,data[i + 256]);
    if((i + 256) == 479){
      break;
    }
  }
}
void read_numberP10(word page, unsigned char *buffer_number){
  byte page_buffer[256];
  _read_page(page, page_buffer);
  for(int i = 0; i < 64; i++){
    buffer_number[i] = page_buffer[i];
  }
}

void read_contentP10(word page1, word page2, unsigned char *buffer_content){
  byte page_buffer[256];
  _read_page(page1, page_buffer);
  for(int i = 0; i < 256; i++){
    buffer_content[i] = page_buffer[i];
  }
  _read_page(page2, page_buffer);
  for(int i = 0; i < 224; i++){
    buffer_content[i+256] = page_buffer[i];
  }
}

void writeBufferSerial(word pageStart, word pageStop, byte *buffer_content, int sizeBuffer){     // buffer_content < 1024 byte. Lay 4 page luu 1 tuyen < 1024 byte.
  //int sizeBuffer = sizeof(buffer_content);
  if((sizeBuffer > 256) && (sizeBuffer <= 512)){
    for(int i = 0; i < 256; i++){
      write_byte(pageStart,i,buffer_content[i]);
    }
    for(int i = 0; i < (sizeBuffer-256); i++){
      write_byte(pageStart+1,i,buffer_content[i + 256]);
    }
  }
  else if((sizeBuffer > 512) && (sizeBuffer <= 768)){
    for(int i = 0; i < 256; i++){
      write_byte(pageStart,i,buffer_content[i]);
    }
    for(int i = 0; i < 256; i++){
      write_byte(pageStart+1,i,buffer_content[i + 256]);
    }
    for(int i = 0; i < (sizeBuffer-256); i++){
      write_byte(pageStart+2,i,buffer_content[i + 512]);
    }
  }
  else{
    for(int i = 0; i < 256; i++){
      write_byte(pageStart,i,buffer_content[i]);
    }
    for(int i = 0; i < 256; i++){
      write_byte(pageStart+1,i,buffer_content[i + 256]);
    }
    for(int i = 0; i < 256; i++){
      write_byte(pageStart+2,i,buffer_content[i + 512]);
    }
    for(int i = 0; i < (sizeBuffer-256); i++){
      write_byte(pageStop,i,buffer_content[i + 768]);
    }
  }
}

void readDataSerial(word pageStart, word pageStop, byte *buffer_content){
  byte page_buffer[256];
  
}

void write_720byte(word page1, word page2, word page3, unsigned char *data){     // ghi 720 byte vao 3 page (thua 48 byte).
  for(int i = 0; i < 256; i++){
    write_byte(page1,i,data[i]);
  }
  for(int i = 0; i < 256; i++){
    write_byte(page2,i,data[i + 256]);
  }
  for(int i = 0; i < 256; i++){
    write_byte(page3,i,data[i + 512]);
    if((i + 512) == 719){
      break;
    }
  }
}

void read_720byte(word page1, word page2, word page3, unsigned char *buffer720){
  //byte buffer720[720];
  byte page_buffer[256];
  _read_page(page1, page_buffer);
  for(int i = 0; i < 256; i++){
    buffer720[i] = page_buffer[i];
  }
  _read_page(page2, page_buffer);
  for(int i = 0; i < 256; i++){
    buffer720[i + 256] = page_buffer[i];
  }
  _read_page(page3, page_buffer);
  for(int i = 0; i < 256; i++){
    buffer720[i + 512] = page_buffer[i];
    if((i + 512) == 719){
      break;
    }
  }
}

void write_504byte(word page1, word page2, unsigned char *data){     // ghi 504 byte vao 3 page (thua 48 byte).
  for(int i = 0; i < 256; i++){
    write_byte(page1,i,data[i]);
  }
  for(int i = 0; i < 248; i++){
    write_byte(page2,i,data[i + 256]);
  }
}

void read_504byte(word page1, word page2, unsigned char *buffer504){
  //byte buffer720[720];
  byte page_buffer[256];
  _read_page(page1, page_buffer);
  for(int i = 0; i < 256; i++){
    buffer504[i] = page_buffer[i];
  }
  _read_page(page2, page_buffer);
  for(int i = 0; i < 248; i++){
    buffer504[i + 256] = page_buffer[i];
  }
}
