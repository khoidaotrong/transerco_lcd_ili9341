
#include "SPI.h"
#include "Adafruit_GFX_AS.h"
#include "Adafruit_ILI9341_STM.h"
#include <GKScroll.h>
#include "logo.h"
//**************************************
#define TFT_CS         PB8
#define TFT_DC         PB6
#define TFT_RST        PB7

#define SD_CS        PB0

Adafruit_ILI9341_STM tft = Adafruit_ILI9341_STM (TFT_CS, TFT_DC, TFT_RST);

//*************************************
#define UP         PA0
#define DOWN       PA3
#define OK         PA1
#define ESC        PA2

#define SEL_0         PB9
#define SEL_1         PB10

#define SEL_E         PB11

#define RS_485_RE  PA8
//*************************************
int btUP, btDOWN, btOK, btESC;
int lbtUP, lbtDOWN, lbtOK, lbtESC;

volatile boolean up = false;
volatile boolean down = false;
volatile boolean ok = false;
volatile boolean esc = false;

int menuitem = 1, menuitem1 = 1;
int checkmenu;
int page = 1;

int vitribatdau = 0;
String chuoi1 = " CHON TUYEN";
String chuoi2 = " XE VE GARA";
String chuoi3 = " XE HUY DONG";



String chuoi11 = " TUYEN 107";
String chuoi12 = " TUYEN 30";
String chuoi13 = " TUYEN 14";
String chuoi14 = " TUYEN 14B";
String chuoi15 = " TUYEN 16";

String chuoi16 = " TUYEN 38";
String chuoi17 = " TUYEN 39";
String chuoi18 = " TUYEN 89";

//
String chuoi19 = " TUYEN 15";
String chuoi110 = " TUYEN 31";
String chuoi111 = " TUYEN 36";
String chuoi112 = " TUYEN 100";
String chuoi113 = " TUYEN 89";

//*********************************

String chuoi11a = " KIM MA -> CNC HOA LAC -> LANG VAN HOA DTVN";
String chuoi11b = " LANG VAN HOA DTVN -> CNC HOA LAC -> KIM MA";



String chuoi12a = " MAI DONG -> BX MY DINH";
String chuoi12b = " BX MY DINH -> MAI DONG";

String chuoi13a = " BO HO -> CO NHUE";
String chuoi13b = " CO NHUE -> BO HO";



String chuoi14a = " TRAN KHANH DU -> CO NHUE";
String chuoi14b = " CO NHUE -> TRAN KHANH DU";


String chuoi15a = " BX MY DINH -> GIA LAM";
String chuoi15b = " GIA LAM -> BX MY DINH";


String chuoi16a = " BX NAM THANG LONG -> MAI DONG";
String chuoi16b = " MAI DONG -> BX NAM THANG LONG";


String chuoi17a = " CV NGHIA DO -> BV NOI TIET TW";
String chuoi17b = " BV NOI TIET TW -> CV NGHIA DO";


String chuoi18a = " BX YEN NGHIA -> BX SON TAY";
String chuoi18b = " BX SON TAY -> BX YEN NGHIA";

String chuoi19a = " BX GIA LAM -> PHO NI";
String chuoi19b = " PHO NI -> BX GIA LAM";

String chuoi110a = " BCAH KHOA -> CHEM (DAI HOC MO)";
String chuoi110b = " CHEM (DAI HOC MO) -> BACH KHOA";

String chuoi111a = " YEN PHU -> KDT LINH DAM";
String chuoi111b = " KDT LINH DAM -> YEN PHU";

String chuoi112a = " LONG BIEN -> KDT DANG XA";
String chuoi112b = " KDT DANG XA -> LONG BIEN";


#define bantin1 "00XVG"
#define bantin2 "00XHD"
#define bantin3 "10701"
#define bantin4 "10702"
#define bantin5 "03001"
#define bantin6 "03002"
#define bantin7 "01401"
#define bantin8 "01402"
#define bantin9 "14B01"
#define bantin10 "14B02"
#define bantin11 "01601"
#define bantin12 "01602"
#define bantin13 "03801"
#define bantin14 "03802"
#define bantin15 "03901"
#define bantin16 "03902"
#define bantin17 "08901"
#define bantin18 "08902"

//
#define bantin19 "01501"
#define bantin20 "01502"
#define bantin21 "03101"
#define bantin22 "03102"
#define bantin23 "03601"
#define bantin24 "03602"
#define bantin25 "10001"
#define bantin26 "10002"
//-------------------------------------------------------------------------------
#define bantinR31 "107R3"
#define bantinR32 "030R3"
#define bantinR33 "014R3"
#define bantinR34 "14BR3"
#define bantinR35 "016R3"
#define bantinR36 "038R3"
#define bantinR37 "039R3"
#define bantinR38 "089R3"

//
#define bantinR39 "015R3"
#define bantinR310 "031R3"
#define bantinR311 "036R3"
#define bantinR312 "100R3"


void setup(void)
{
  pinMode(PB5, OUTPUT);
  digitalWrite(PB5, 1);
  delay(1);

  pinMode(SD_CS, OUTPUT);
  digitalWrite(SD_CS, 0);
  delay(500);

  Serial1.begin(9600);
  // Set up period

  afio_cfg_debug_ports(AFIO_DEBUG_SW_ONLY);

  pinMode(SEL_0, OUTPUT);
  pinMode(SEL_1, OUTPUT);
  pinMode(SEL_E, OUTPUT);
  digitalWrite(SEL_E, 0);

  digitalWrite(SEL_0, 0);
  digitalWrite(SEL_1, 0);

  pinMode(UP, INPUT_PULLUP);
  pinMode(DOWN, INPUT_PULLUP);
  pinMode(OK, INPUT_PULLUP);
  pinMode(ESC, INPUT_PULLUP);
  pinMode(RS_485_RE, OUTPUT);
  digitalWrite(RS_485_RE, 1);
  pinMode(TFT_RST, OUTPUT);
  digitalWrite(TFT_RST, 0);
  delay(500);
  digitalWrite(TFT_RST, 1);
  delay(500);
  tft.begin();
  tft.setRotation(2);
  drawlogo();
  HardwareTimer timer(2);
  timer.pause();
  timer.setPeriod(100000);
  timer.setChannel1Mode(TIMER_OUTPUT_COMPARE);
  timer.setCompare(TIMER_CH1, 1);
  timer.attachCompare1Interrupt(checktime);
  timer.refresh();

  timer.resume();
  //  //Serial1.end();
}

void chuyen_Serial(int sel) {
    delay(100);
  if (sel == 0) {
    digitalWrite(SEL_1, 0);
    digitalWrite(SEL_0, 0);
  }
  if (sel == 1) {
    digitalWrite(SEL_1, 0);
    digitalWrite(SEL_0, 1);
  }
  if (sel == 2) {
    digitalWrite(SEL_1, 1);
    digitalWrite(SEL_0, 0);
  }
  if (sel == 3) {
    digitalWrite(SEL_1, 1);
    digitalWrite(SEL_0, 1);
  }
  delay(50);

}

int dem2 = 0, seconds = 30;
void checktime()
{
  if (esc == true) {
    seconds = 30;
    esc = false;
  }
  seconds++;
  if (seconds == 100) seconds = 30;
  if (dem2 == 1)
  {
    tft.fillRect(0, 200, 240, 70, ILI9341_BLACK);
    tft.setCursor (40, 200);
    tft.print("SENDING...");
    seconds = 0;
    dem2 = 0;
  }
  if (seconds == 5)
  {
    if (esc == true) {
      seconds = 30;
      esc = false;
    }
    tft.fillRect(0, 200, 320, 40, ILI9341_BLACK);
    tft.fillRect(0, 230, 320, 40, ILI9341_BLACK);
    tft.setCursor (40, 230);
    tft.print("SUCCESS");
  }
  if (seconds == 10)
  {
    if (esc == true) {
      seconds = 30;
      esc = false;
    }
    tft.fillRect(0, 200, 320, 40, ILI9341_BLACK);
    tft.fillRect(0, 230, 320, 40, ILI9341_BLACK);
  }
}

//---------------------------------------------------------------------------------
void loop()
{
  drawmenuscroll();

  btUP =  digitalRead(UP);
  btDOWN =  digitalRead(DOWN);
  btOK =  digitalRead(OK);
  btESC =  digitalRead(ESC);

  checkUP();
  checkDOWN();
  checkOK();
  checkESC();

}


//----------------------------------------------------------------------------------

void checkUP()
{

  //************************
  if (page == 1)
  {
    if (btUP != lbtUP)
    {
      if (btUP == 0)
      {
        up = true;

      }
      delay(1);
      if (up == 1)
      {
        up = false;
        menuitem--;
        if (menuitem == 0)
          menuitem = 3;
        drawmainmenu();
      }
    }
    lbtUP = btUP;
  }

  //****************************
  if (page == 2)
  {
    if (btUP != lbtUP)
    {
      if (btUP == 0)
      {
        up = true;

      }
      delay(1);
      if (up == 1)
      {
        up = false;
        menuitem--;
        if (menuitem == 0)
        {
          menuitem = 8;
          tft.fillScreen(ILI9341_BLACK);
        }
        if (menuitem == 8) tft.fillScreen(ILI9341_BLACK);
        drawmainmenu();
      }
    }
    lbtUP = btUP;
  }


  if (page >= 3)
  {
    if (btUP != lbtUP)
    {
      if (btUP == 0)
      {
        up = true;

      }
      delay(1);
      if (up == 1)
      {
        up = false;
        menuitem--;
        if (menuitem == 0)
          menuitem = 2;
        drawmainmenu();
      }
    }
    lbtUP = btUP;
  }
}

void checkDOWN()
{
  //**************************************
  if (page == 1)
  {
    if (btDOWN != lbtDOWN)
    {
      if (btDOWN == 0)
      {
        down = true;

      }
      delay(1);
      if (down == 1)
      {
        down = false;
        menuitem++;
        if (menuitem == 4) menuitem = 1;
        drawmainmenu();
      }
    }
    lbtDOWN = btDOWN;
  }

  //**************************************
  if (page == 2)
  {
    if (btDOWN != lbtDOWN)
    {
      if (btDOWN == 0)
      {
        down = true;

      }
      delay(1);
      if (down == 1)
      {
        down = false;
        menuitem++;
        if (menuitem == 9) {
          menuitem = 1;
          tft.fillScreen(ILI9341_BLACK);
        }
        if (menuitem == 8) tft.fillScreen(ILI9341_BLACK);
        drawmainmenu();
      }
    }
    lbtDOWN = btDOWN;
  }


  if (page >= 3)
  {
    if (btDOWN != lbtDOWN)
    {
      if (btDOWN == 0)
      {
        down = true;

      }
      delay(1);
      if (down == 1)
      {
        down = false;
        menuitem++;
        if (menuitem == 3) menuitem = 1;
        drawmainmenu();
      }
    }
    lbtDOWN = btDOWN;
  }
}

void checkOK()
{
  if (btOK != lbtOK)
  {
    if (btOK == 0)
    {
      ok = true;

      if ((page == 2) && (menuitem == 1))
      {
        for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.print(bantinR31);
        }
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 3;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();

      }
      else if ((page == 2) && (menuitem == 2))
      {
        for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantinR32);
        }
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 4;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
      else if ((page == 2) && (menuitem == 3))
      {
        for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantinR33);
        }
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 5;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
      else if ((page == 2) && (menuitem == 4))
      {
        for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantinR34);
        }
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 6;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
      else if ((page == 2) && (menuitem == 5))
      {
        for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantinR35);
        }
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 7;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
      else if ((page == 2) && (menuitem == 6))
      {
        for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantinR36);
        }
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 8;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
      else if ((page == 2) && (menuitem == 7))
      {
        for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantinR37);
        }
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 9;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }

      else if ((page == 2) && (menuitem == 8))
      {
        for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantinR38);
        }
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 10;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
      else if ((page == 2) && (menuitem == 9))
      {
        for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantinR39);
        }
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 11;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
      else if ((page == 2) && (menuitem == 10))
      {
        for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantinR310);
        }
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 12;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
      else if ((page == 2) && (menuitem == 11))
      {
        for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantinR311);
        }
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 13;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
      else if ((page == 2) && (menuitem == 12))
      {
        for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantinR312);
        }
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 14;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }

      else if ((page == 1) && (menuitem == 1))
      {
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 2;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
    }
    delay(25);
  }
  lbtOK = btOK;
}

void checkESC()
{
  if (btESC != lbtESC)
  {
    if (btESC == 0)
    {
      tft.setTextColor(ILI9341_RED);
      tft.setTextSize(2);
      tft.setCursor (135, 290);
      tft.print("cancel");
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(3);
      esc = true;
      menuitem = 0;
      if (page == 1) drawlogo();
      if (page == 2) {
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        page = 1;
      }
      if (page >= 3) page = 2;
      drawmainmenu();
      menuitem = 1;
    }
    delay(25);
  }
  lbtESC = btESC;
}
//****************************************************

void drawlogo()
{

  int dem = 0, dem1 = 0;
  tft.fillScreen(ILI9341_WHITE);
  for (int i = 0; i < 64; i++)
    for (int j = 0; j < 70; j++)
    {
      dem1 = pgm_read_word_near(demo + 2 * dem) * 256 + pgm_read_word_near(demo + 2 * dem + 1);
      for (int k_1 = 0; k_1 < 2; k_1++)
        for (int k_2 = 0; k_2 < 2; k_2++)
        {
          tft.drawPixel(50 + j * 2 + k_1, 20 + i * 2 + k_2, dem1) ;
        }
      dem++;
      if (dem == 6080) dem = 0;
    };
  tft.drawBitmap(35, 168, bitmap3, 168, 21, ILI9341_ORANGE);
  tft.drawBitmap(35, 216 , bitmap2, 167, 25, ILI9341_BLUE);
  tft.setCursor (10, 280);
  tft.setTextColor(ILI9341_BLACK);
  tft.setTextSize(3);
  tft.print("MENU");
  while (1)
  {
    if (digitalRead(OK) == 0) break;
  };
  tft.fillScreen(ILI9341_BLACK);
  tft.setTextColor(ILI9341_WHITE);
  tft.setTextSize(3);
  drawmainmenu();
  delay(10);
}

void drawmainmenu()
{
  //***************************** page1
  if (page == 1)
  {
    if (menuitem == 1)
    {
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi1.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin1);         }

      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi2.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 3)
    {
      if ((menuitem == 3) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin2);         }
      }
    }
    else
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
      tft.setCursor (10, 90);
      tft.print("3.");
      tft.setCursor (40, 90);
      tft.print(chuoi3.substring(0, 10));
      delay(50);
    }

    tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
  }
  //****************************************************** PAGE 2
  if (page == 2)
  {
    if ((menuitem >= 8) && (menuitem <= 12)) {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
      tft.setCursor (10, 10);
      tft.print("8.");
      tft.setCursor (40, 10);
      tft.print(chuoi18.substring(0, 10));
      delay(50);
      if (menuitem != 8) {
        tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
        tft.setCursor (10, 10);
        tft.print("8.");
        tft.setCursor (40, 10);
        tft.print(chuoi18.substring(0, 10));
        delay(50);
      }
//      if (menuitem != 9) {
//        tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
//        tft.setCursor (10, 50);
//        tft.print("9.");
//        tft.setCursor (40, 50);
//        tft.print(chuoi19.substring(0, 10));
//        delay(50);
//      }
//      if (menuitem != 10) {
//        tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
//        tft.setCursor (10, 90);
//        tft.print("10.");
//        tft.setCursor (40, 90);
//        tft.print(chuoi110.substring(0, 10));
//        delay(50);
//      }
//      if (menuitem != 11) {
//        tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
//        tft.setCursor (10, 130);
//        tft.print("11.");
//        tft.setCursor (40, 130);
//        tft.print(chuoi111.substring(0, 10));
//        delay(50);
//      }
//
//      if (menuitem != 12) {
//        tft.fillRect(0, 160, 320, 40, ILI9341_BLACK);
//        tft.setCursor (10, 170);
//        tft.print("12.");
//        tft.setCursor (40, 170);
//        tft.print(chuoi112.substring(0, 10));
//        delay(50);
//      }

      //*************************************** khung man hinh
      tft.setTextSize(2);
      tft.drawRect(5, 280, 115, 40, ILI9341_WHITE);
      tft.setCursor (35, 290);
      tft.print("ok");
      tft.drawRect(120, 280, 115, 40, ILI9341_WHITE);
      tft.setCursor (135, 290);
      tft.print("cancel");
      tft.setTextSize(3);
      //  tft.drawRect(111, 270, 220, 250, ILI9341_WHITE);
      return;
    }
    else
    {

    }
    if (menuitem == 1) {}
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi11.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)  {}
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi12.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 3) {}
    else
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 90);
    tft.print("3.");
    tft.setCursor (40, 90);
    tft.print(chuoi13.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    if (menuitem == 4) {}
    else
    {
      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 130);
    tft.print("4.");
    tft.setCursor (40, 130);
    tft.print(chuoi14.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    if (menuitem == 5) {}
    else
    {
      tft.fillRect(0, 160, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 170);
    tft.print("5.");
    tft.setCursor (40, 170);
    tft.print(chuoi15.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    if (menuitem == 6) {}
    else
    {
      tft.fillRect(0, 200, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 210);
    tft.print("6.");
    tft.setCursor (40, 210);
    tft.print(chuoi16.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    if (menuitem == 7) {}
    else
    {
      tft.fillRect(0, 240, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 250);
    tft.print("7.");
    tft.setCursor (40, 250);
    tft.print(chuoi17.substring(0, 10));
    delay(50);
    //    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
  }



  //****************************************************** PAGE 3
  if (page == 3)
  {
    if (menuitem == 1) {}
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi11a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin4);
        }
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi11b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin5);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi11c.substring(0, 10));
    //      delay(50);
    //    }
    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin6);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi11d.substring(0, 10));
    //      delay(50);
    //    }
  }

  //************************************************* page 4
  if (page == 4)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin7);
        }
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi12a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin8);         }
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi12b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin9);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi12c.substring(0, 10));
    //      delay(50);
    //    }
    //    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin10);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi12d.substring(0, 10));
    //      delay(50);
    //    }
  }
  //************************************************* page 5
  if (page == 5)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin11);         }
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi13a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin12);         }
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi13b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin13);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi13c.substring(0, 10));
    //      delay(50);
    //    }
    //    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin14);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi13d.substring(0, 10));
    //      delay(50);
    //    }
  }

  //*&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*******************************$&&&&&&&&&&&&&&&&% page6
  if (page == 6)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin11);         }
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi14a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin12);         }
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi14b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin13);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi14c.substring(0, 10));
    //      delay(50);
    //    }
    //    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin14);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi14d.substring(0, 10));
    //      delay(50);
    //    }
  }
  //*&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*******************************$&&&&&&&&&&&&&&&&%
  if (page == 7)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin11);         }
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi15a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin12);
        }
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi15b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin13);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi15c.substring(0, 10));
    //      delay(50);
    //    }
    //    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin14);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi15d.substring(0, 10));
    //      delay(50);
    //    }
  }
  //*&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*******************************$&&&&&&&&&&&&&&&&%
  if (page == 8)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin11);         }
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi16a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin12);         }
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi16b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin13);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi16c.substring(0, 10));
    //      delay(50);
    //    }
    //    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin14);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi16d.substring(0, 10));
    //      delay(50);
    //    }
  }
  //*&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*******************************$&&&&&&&&&&&&&&&&%
  if (page == 9)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin11);         }
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi17a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin12);         }
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi17b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    ////        ok = false;
    ////        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin13);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi17c.substring(0, 10));
    //      delay(50);
    //    }
    //    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    ////        ok = false;
    ////        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin14);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi17d.substring(0, 10));
    //      delay(50);
    //    }
  }
  if (page == 10)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin11);         }
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi18a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin12);         }
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi18b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
  }

  if (page == 11)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin11);         }
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi19a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin12);         }
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi19b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    ////        ok = false;
    ////        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin13);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi17c.substring(0, 10));
    //      delay(50);
    //    }
    //    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    ////        ok = false;
    ////        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin14);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi17d.substring(0, 10));
    //      delay(50);
    //    }
  }

  if (page == 12)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin11);         }
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi110a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin12);         }
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi110b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    ////        ok = false;
    ////        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin13);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi17c.substring(0, 10));
    //      delay(50);
    //    }
    //    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    ////        ok = false;
    ////        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin14);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi17d.substring(0, 10));
    //      delay(50);
    //    }
  }

  if (page == 13)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin11);         }
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi111a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin12);         }
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi111b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    ////        ok = false;
    ////        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin13);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi17c.substring(0, 10));
    //      delay(50);
    //    }
    //    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    ////        ok = false;
    ////        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin14);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi17d.substring(0, 10));
    //      delay(50);
    //    }
  }

  if (page == 14)
  {
    if (menuitem == 1)
    {
      if ((menuitem == 1) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin11);         }
      }
    }
    else
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 10);
    tft.print("1.");
    tft.setCursor (40, 10);
    tft.print(chuoi112a.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      if ((menuitem == 2) && (ok == 1))
      {
        //        ok = false;
        //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin12);         }
      }
    }
    else
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_BLACK);
    }
    tft.setCursor (10, 50);
    tft.print("2.");
    tft.setCursor (40, 50);
    tft.print(chuoi112b.substring(0, 10));
    delay(50);
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 3)
    //    {
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    ////        ok = false;
    ////        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin13);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(chuoi17c.substring(0, 10));
    //      delay(50);
    //    }
    //    ////*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //    if (menuitem == 4)
    //    {
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    ////        ok = false;
    ////        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin14);         }
    //      }
    //    }
    //    else
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_BLACK);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(chuoi17d.substring(0, 10));
    //      delay(50);
    //    }
  }


  //*&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*******************************$&&&&&&&&&&&&&&&&%

  //*************************************** khung man hinh
  tft.setTextSize(2);
  tft.drawRect(5, 280, 115, 40, ILI9341_WHITE);
  tft.setCursor (35, 290);
  tft.print("ok");
  tft.drawRect(120, 280, 115, 40, ILI9341_WHITE);
  tft.setCursor (135, 290);
  tft.print("cancel");
  tft.setTextSize(3);
  //  tft.drawRect(111, 270, 220, 250, ILI9341_WHITE);
}

/*//////////////////////////////////////%^&($(%&))#)#)#)#)_$__%%(&@)#)#(@)__%(#)%#()//////////////////////////////////////////////////////////////
  //////////////////////////////////////%^&($(%&))#)#)#)#)_$__%%(&@)#)#(@)__%(#)%#()//////////////////////////////////////////////////////////////
  //////////////////////////////////////%^&($(%&))#)#)#)#)_$__%%(&@)#)#(@)__%(#)%#()////////////////////////////////////////////////////////////////////////////////////////////////////%^&($(%&))#)#)#)#)_$__%%(&@)#)#(@)__%(#)%#()//////////////////////////////////////////////////////////////
  //////////////////////////////////////%^&($(%&))#)#)#)#)_$__%%(&@)#)#(@)__%(#)%#()//////////////////////////////////////////////////////////////
  //////////////////////////////////////%^&($(%&))#)#)#)#)_$__%%(&@)#)#(@)__%(#)%#()//////////////////////////////////////////////////////////////*/

void drawmenuscroll()
{
  //  if(page==0) drawlogo();
  //********************************************
  if (page == 1)
  {

    //*******************************************
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (45, 10);
      tft.print(ScrollLine(chuoi1, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi2, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin1);
        }
        //Serial1.end();
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 3)
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
      tft.setCursor (10, 90);
      tft.print("3.");
      tft.setCursor (40, 90);
      tft.print(ScrollLine(chuoi3, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin2);
        }
        //Serial1.end();
      }
    }
    //************************************
    // if(menuitem ==4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //     tft.print("4.");
    //     tft.setCursor (40, 130);
    //     tft.print(ScrollLine(chuoi4, 5, 10));
    //    delay(100);
    //      if((menuitem ==4)&&(ok ==1))
    //      {
    //        ok = false;
    //        dem2=1;Serial1.write("4");
    //      }
    //    }
  }

  //***********************************************************//////////////////////  PAGE 2
  if (page == 2)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi11, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi12, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 3)
    {
      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
      tft.setCursor (10, 90);
      tft.print("3.");
      tft.setCursor (40, 90);
      tft.print(ScrollLine(chuoi13, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
      }
    }
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 4)
    {
      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
      tft.setCursor (10, 130);
      tft.print("4.");
      tft.setCursor (40, 130);
      tft.print(ScrollLine(chuoi14, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
      }
    }
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 5)
    {
      tft.fillRect(0, 160, 320, 40, ILI9341_RED);
      tft.setCursor (10, 170);
      tft.print("5.");
      tft.setCursor (40, 170);
      tft.print(ScrollLine(chuoi15, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
      }
    }
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 6)
    {
      tft.fillRect(0, 200, 320, 40, ILI9341_RED);
      tft.setCursor (10, 210);
      tft.print("6.");
      tft.setCursor (40, 210);
      tft.print(ScrollLine(chuoi16, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
        //        //        dem2=1;Serial1.write("3");
      }
    }
    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 7)
    {
      tft.fillRect(0, 240, 320, 40, ILI9341_RED);
      tft.setCursor (10, 250);
      tft.print("7.");
      tft.setCursor (40, 250);
      tft.print(ScrollLine(chuoi17, 5, 10));
      delay(100);
      if ((menuitem == 3) && (ok == 1))
      {
        ok = false;
        //        //        dem2=1;Serial1.write("3");
      }
    }
    if (menuitem == 8)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("8.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi18, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
      }
    }
//    if (menuitem == 9)
//    {
//      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 50);
//      tft.print("9.");
//      tft.setCursor (40, 50);
//      tft.print(ScrollLine(chuoi19, 5, 10));
//      delay(100);
//      if ((menuitem == 1) && (ok == 1))
//      {
//        ok = false;
//      }
//    }
//    if (menuitem == 10)
//    {
//      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 90);
//      tft.print("10.");
//      tft.setCursor (40, 90);
//      tft.print(ScrollLine(chuoi110, 5, 10));
//      delay(100);
//      if ((menuitem == 1) && (ok == 1))
//      {
//        ok = false;
//      }
//    }
//    if (menuitem == 11)
//    {
//      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 130);
//      tft.print("11.");
//      tft.setCursor (40, 130);
//      tft.print(ScrollLine(chuoi111, 5, 10));
//      delay(100);
//      if ((menuitem == 1) && (ok == 1))
//      {
//        ok = false;
//      }
//    }
//    if (menuitem == 12)
//    {
//      tft.fillRect(0, 160, 320, 40, ILI9341_RED);
//      tft.setCursor (10, 170);
//      tft.print("12.");
//      tft.setCursor (40, 170);
//      tft.print(ScrollLine(chuoi112, 5, 10));
//      delay(100);
//      if ((menuitem == 1) && (ok == 1))
//      {
//        ok = false;
//      }
//    }

  }
  //***************************************************************
  if (page == 3)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi11a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin3);
        }

        //Serial1.end();
        //        if(page==1) page=2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi11b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin4);
        }
        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi11c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin5);         }
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi11d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin6);         }
    //      }
    //    }
  }

  //***************************************************************
  if (page == 4)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi12a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin5);
        }

        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi12b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin6);
        }

        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi12c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin9);         }
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi12d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin10);         }
    //      }
    //    }
  }
  //***************************************************************
  if (page == 5)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi13a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin7);
        }

        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi13b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin8);
        }

        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi13c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin13);         }
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi13d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; for (int i = 0; i < 3; i++)         {            chuyen_Serial(i);           delay(100);           Serial1.write(bantin14);         }
    //      }
    //    }
  }
  //%*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&%**************************************************************
  if (page == 6)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi14a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin9);
        }

        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi14b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin10);
        }

        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi14c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0983");
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi14d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0984");
    //      }
    //    }
  }
  //%*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&%**************************************************************
  if (page == 7)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi15a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin11);
        }

        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi15b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin12);
        }
        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi15c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0993");
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi15d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0994");
    //      }
    //    }
  }
  //%*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&%**************************************************************
  if (page == 8)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi16a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin13);
        }
        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi16b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin14);
        }
        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi16c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0643");
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi16d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0644");
    //      }
    //    }
  }
  //%*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&%**************************************************************
  if (page == 9)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi17a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin15);
        }
        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi17b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin16);
        }

        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi17c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0653");
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi17d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0654");
    //      }
    //    }
  }
  if (page == 10)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi18a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin17);
        }
        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi18b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin18);
        }

        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi17c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0653");
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi17d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0654");
    //      }
    //    }
  }
  if (page == 11)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi19a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; for (int i = 0; i < 3; i++)         {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin19);
        }
        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi19b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; for (int i = 0; i < 3; i++)
        {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin20);
        }

        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi17c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0653");
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi17d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0654");
    //      }
    //    }
  }
  if (page == 12)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi110a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; for (int i = 0; i < 3; i++)
        {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin21);
        }
        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi110b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; for (int i = 0; i < 3; i++)
        {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin22);
        }

        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi17c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0653");
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi17d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0654");
    //      }
    //    }
  }
  if (page == 13)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi111a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; for (int i = 0; i < 3; i++)
        {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin23);
        }
        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi111b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1; for (int i = 0; i < 3; i++)
        {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin24);
        }

        //Serial1.end();
      }
    }
    //***********************************************
    //    if (menuitem == 3)
    //    {
    //      tft.fillRect(0, 80, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 90);
    //      tft.print("3.");
    //      tft.setCursor (40, 90);
    //      tft.print(ScrollLine(chuoi17c, 5, 10));
    //      delay(100);
    //      if ((menuitem == 3) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0653");
    //      }
    //    }
    //    //************************************
    //    if (menuitem == 4)
    //    {
    //      tft.fillRect(0, 120, 320, 40, ILI9341_RED);
    //      tft.setCursor (10, 130);
    //      tft.print("4.");
    //      tft.setCursor (40, 130);
    //      tft.print(ScrollLine(chuoi17d, 5, 10));
    //      delay(100);
    //      if ((menuitem == 4) && (ok == 1))
    //      {
    //        ok = false;
    //        dem2 = 1; Serial1.write("0654");
    //      }
    //    }
  }
  if (page == 14)
  {
    if (menuitem == 1)
    {
      tft.fillRect(0, 0, 320, 40, ILI9341_RED);
      tft.setCursor (10, 10);
      tft.print("1.");
      tft.setCursor (40, 10);
      tft.print(ScrollLine(chuoi112a, 5, 10));
      delay(100);
      if ((menuitem == 1) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);
        dem2 = 1; for (int i = 0; i < 3; i++)
        {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin25);
        }
        //Serial1.end();
        if (page == 1) page = 2;
      }
    }

    //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    if (menuitem == 2)
    {
      tft.fillRect(0, 40, 320, 40, ILI9341_RED);
      tft.setCursor (10, 50);
      tft.print("2.");
      tft.setCursor (40, 50);
      tft.print(ScrollLine(chuoi112b, 5, 10));
      delay(100);
      if ((menuitem == 2) && (ok == 1))
      {
        ok = false;
        //Serial1.begin(9600);

        dem2 = 1;
        for (int i = 0; i < 3; i++)
        {
          chuyen_Serial(i);
          delay(100);
          Serial1.write(bantin26);
        }

        //Serial1.end();
      }
    }
    //***********************************************

  }
}
