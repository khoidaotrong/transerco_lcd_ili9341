#include "SPI.h"
#include "Adafruit_GFX_AS.h"
#include "Adafruit_ILI9341_STM.h"
#include <GKScroll.h>
#include "logo.h"


#define UP         PA3
#define DOWN       PA1
#define OK         PA0
#define ESC        PA2

#define RS_485_RE  PA8

#define TFT_CS         PA15
#define TFT_RS         PB4
#define TFT_RST        PB3
//#define TFT_SDI PA5  // MOSI
//#define TFT_CLK PA7  // SCK
#define TFT_LED        PA12   

Adafruit_ILI9341_STM tft = Adafruit_ILI9341_STM (TFT_CS, TFT_DC, TFT_RST);

int btUP, btDOWN, btOK, btESC;
int lbtUP, lbtDOWN, lbtOK, lbtESC;

volatile boolean up = false;
volatile boolean down = false;
volatile boolean ok = false;
volatile boolean esc = false;


int menuitem = 1, menuitem2 = 1, menuitem3 = 1;
int page = 1;
const int menuitem_max_2 = 5, 
          menuitem_max_3 = 2;
          //max_2 la so dong cua page2, max_3 la so dong page>3
int page_max = 7;

//chuoi hien thi
String ky_tu_dau[7] = {
  "1.  ", "2.  ", "3.  ", "4.  ", "5.  ", "6.  ", "7.  "
};

String page_1[3] = {
  "CHON TUYEN", "XE HUY DONG", "XE VE GARA"
};

String ten_tuyen[menuitem_max_2]={
    "TUYEN 1", "TUYEN 2", "TUYEN 3","TUYEN 4", "TUYEN 5"
};

String chieu_di[menuitem_max_2]={
  "CHIEU DI 1",
  "CHIEU DI 2",
  "CHIEU DI 3",
  "CHIEU DI 4",
  "CHIEU DI 5"
}

String ban_tin_di[menuitem_max_2]={
  "CHIEU DI 1",
  "CHIEU DI 2",
  "CHIEU DI 3",
  "CHIEU DI 4",
  "CHIEU DI 5"
}

String chieu_ve[menuitem_max_2]={
  "CHIEU VE 1",
  "CHIEU VE 2",
  "CHIEU VE 3",
  "CHIEU VE 4",
  "CHIEU VE 5"
}

String ban_tin_ve[menuitem_max_2]={
  "CHIEU VE 1",
  "CHIEU VE 2",
  "CHIEU VE 3",
  "CHIEU VE 4",
  "CHIEU VE 5"
}

void checkUP()
{
  if (page == 1)
  {
    if (btUP != lbtUP)
    {
      if (btUP == 0)
      {
        up = true;
      }
      if (up == 1)
      {
        up = false;
        menuitem--;
        if (menuitem == 0)
          menuitem = 3;
        drawmainmenu();
      }
    }
    lbtUP = btUP;
  }
  
  //****************************
  if (page == 2)
  {
    if (btUP != lbtUP)
    {
      if (btUP == 0)
      {
        up = true;
      }
      if (up == 1)
      {
        up = false;
        menuitem--;
        if (menuitem == 0)
          menuitem = menuitem_max_2;
        drawmainmenu();
      }
    }
    lbtUP = btUP;
  }

  
  if ((page>=3)&&(page<=page_max))
  {
    if (btUP != lbtUP)
    {
      if (btUP == 0)
      {
        up = true;
      }
      if (up == 1)
      {
        up = false;
        menuitem--;
        if (menuitem == 0)
          menuitem = menuitem_max_3;
        drawmainmenu();
      }
    }
    lbtUP = btUP;
  }
}

void checkDOWN()
{
  //**************************************
  if (page == 1)
  {
    if (btDOWN != lbtDOWN)
    {
      if (btDOWN == 0)
      {
        down = true;
      }
      if (down == 1)
      {
        down = false;
        menuitem++;
        if (menuitem == 4) menuitem = 1;
        drawmainmenu();
      }
    }
    lbtDOWN = btDOWN;
  }

  //**************************************
  if (page == 2)
  {
    if (btDOWN != lbtDOWN)
    {
      if (btDOWN == 0)
      {
        down = true;
      }
      if (down == 1)
      {
        down = false;
        menuitem++;
        if (menuitem == (menuitem_max_2+1)) menuitem = 1;
        drawmainmenu();
      }
    }
    lbtDOWN = btDOWN;
  }
  if ((page>=3)&&(page<=page_max))
  {
    if (btDOWN != lbtDOWN)
    {
      if (btDOWN == 0)
      {
        down = true;

      }
      delay(1);
      if (down == 1)
      {
        down = false;
        menuitem++;
        if (menuitem == (menuitem_max_3+1)) menuitem = 1;
        drawmainmenu();
      }
    }
    lbtDOWN = btDOWN;
  }
}

void checkOK()
{
  if (btOK != lbtOK)
  {
    if (btOK == 0)
    {
      ok = true;
      
      if (page == 2)
      {
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = menuitem+2;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
     
      else if ((page == 1) && (menuitem == 1))
      {
        ok = false;
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(2);
        tft.setCursor (35, 290);
        tft.print("ok");
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(3);
        page = 2;
        menuitem = 1;
        tft.fillRect(0, 0, 320, 280, ILI9341_BLACK);
        drawmainmenu();
      }
    }
  }
  lbtOK = btOK;
}

void checkESC()
{
  if (btESC != lbtESC)
  {
    if (btESC == 0)
    {
      tft.setTextColor(ILI9341_RED);
      tft.setTextSize(2);
      tft.setCursor (135, 290);
      tft.print("cancel");
      tft.setTextColor(ILI9341_WHITE);
      tft.setTextSize(3);
      esc = true;
      menuitem = 0;
      if (page == 1) drawlogo();
      if (page == 2) { tft.fillRect(0, 0, 320, 280, ILI9341_BLACK); page = 1;}
      if ((page>=3)&&(page<=9)) page = 2;
      drawmainmenu();
      menuitem = 1;
    }
  }
  lbtESC = btESC;
}

void drawlogo()
{
  int dem = 0, dem1 = 0;
  tft.fillScreen(ILI9341_WHITE);
  for (int i = 0; i < 72; i++)
    for (int j = 0; j < 76; j++)
    {
      dem1 = pgm_read_word_near(demo + 2 * dem) * 256 + pgm_read_word_near(demo + 2 * dem + 1);
      tft.drawPixel(85 + j, 20 + i, dem1) ;
      dem++;
      if (dem == 6080) dem = 0;
    };
  tft.drawBitmap(35, 160, bitmap3, 168, 21, ILI9341_ORANGE);
  tft.drawBitmap(35, 200 , bitmap2, 167, 25, ILI9341_BLUE);
  tft.setCursor (10, 280);
  tft.setTextColor(ILI9341_BLACK);
  tft.setTextSize(3);
  tft.print("MENU");
  while (1)
  {
    if (digitalRead(OK) == 0) break;
  };
  tft.fillScreen(ILI9341_BLACK);
  tft.setTextColor(ILI9341_WHITE);
  tft.setTextSize(3);
  drawmainmenu();
  delay(10);
}

void setup() {
  // put your setup code here, to run once:
  Serial1.begin(115200);
  // Set up period

  afio_cfg_debug_ports(AFIO_DEBUG_SW_ONLY);

  pinMode(UP, INPUT_PULLUP);
  pinMode(DOWN, INPUT_PULLUP);
  pinMode(OK, INPUT_PULLUP);
  pinMode(ESC, INPUT_PULLUP);
  
  pinMode(RS_485_RE, OUTPUT);
  digitalWrite(RS_485_RE, 1);
  
  pinMode(TFT_RST, OUTPUT);
  digitalWrite(TFT_RST,0);
  delay(300);
  digitalWrite(TFT_RST,1);
  delay(300);
  tft.begin();
  tft.setRotation(2);
  drawlogo();
}

void loop() {
  // put your main code here, to run repeatedly:
  drawmenuscroll();

  btUP =  digitalRead(UP);
  btDOWN =  digitalRead(DOWN);
  btOK =  digitalRead(OK);
  btESC =  digitalRead(ESC);

  checkUP();
  checkDOWN();
  checkOK();
  checkESC();
}
